module Update_J

export WormMoveData,shift_worm!,jump_worm!

import RandomNumbers
using ..SimParameters
using ..Defs
using ..LatticeData
using ..DisorderData
# using Base.Cartesian

struct WormMoveData
  weights_inc::Array{Float64,1}
  weights_dec::Array{Float64,1}
  weights_self_inc::Array{Float64,1}
  weights_self_dec::Array{Float64,1}
  rng::RandomNumbers.MersenneTwisters.MT19937
  function WormMoveData(sim_params::SimParams,rng::RandomNumbers.MersenneTwisters.MT19937)
    w_inc,w_dec,weights_self_inc,weights_self_dec=calc_ratios(sim_params)
    new(w_inc,w_dec,weights_self_inc,weights_self_dec,rng)
  end
end

# """
# calc_loop(m_sqr,lambda):Computes bond weight ratios for a given patameter up to order MAX_BOND
# """

function calc_ratios(sim_params::SimParams)

  coupling1 = 1/(2. *sim_params.lambda2)
  energy_inc = coupling1*[ ((n+1)^2. -n^2.) for n in -MAX_BOND_J:MAX_BOND_J]
  weights_inc= exp.(-energy_inc)
  energy_dec = coupling1*[ ((n-1)^2. -n^2.) for n in -MAX_BOND_J:MAX_BOND_J]
  weights_dec= exp.(-energy_dec)

  coupling2 = 1/(2. *sim_params.lambda3)
  energy_inc = coupling2*[ ((n+1)^2. -n^2.) for n in -MAX_BOND_J:MAX_BOND_J]
  weights_self_inc= exp.(-energy_inc)
  energy_dec = coupling2*[ ((n-1)^2. -n^2.) for n in -MAX_BOND_J:MAX_BOND_J]
  weights_self_dec= exp.(-energy_dec)

  return weights_inc, weights_dec, weights_self_inc, weights_self_dec
end
#
# """
# update_energy:update the energy of the lattice after the worm moves
# """

function update_energy!(lat::Lattice,params::SimParams,disorder::Disorder, dir::Int64, lr::Int64,location::NTuple{NDIMS,Int64})
    nn = lr == 1 ? location : hop(location, dir, 2, size(lat.angle))
    bond_val=lat.current[nn...,dir] #current variable
    c_val = lat.curl_lattice[nn...,dir] #curl variable
    effective_bond = bond_val-params.inter*c_val #
    dE = 0.0
    ## Binding
    if lr ==1
        dE1 = (effective_bond+1)^2. -(effective_bond)^2.
    elseif lr == 2
        dE1 = (effective_bond-1)^2. -(effective_bond)^2.
    else
        println("Error")
    end

    if dir==NDIMS
        coupling = 1/(2*params.lambda2)
    elseif dir==1 || dir==2
        coupling = 1/(2*disorder.bond_disorder_loop[nn[1],nn[2],dir])
    else
        println("Error")
    end

    dE += dE1*coupling

    ## self
    # if lr ==1
    #     dE2 = (bond_val+1)^2-(bond_val)^2
    # elseif lr == 2
    #     dE2 = (bond_val-1)^2-(bond_val)^2
    # else
    #     println("Error")
    # end
    # dE += dE2/(2*params.lambda3)

    # chemical potential
    if dir == NDIMS
        if lr ==1
            dE3 = 1.0*disorder.chemical_potential[nn[1],nn[2]]/params.lambda2
        elseif lr ==2
            dE3 = (-1.0)*disorder.chemical_potential[nn[1],nn[2]]/params.lambda2
        else
            println("Error")
        end
        dE += dE3
    end

    #return nothing
    return dE
end




"""
    shift_worm(WormData,Lattice)

single shift move of worm's head
"""
function shift_worm!(worm_data::WormMoveData,lat::Lattice,params::SimParams,disorder::Disorder)

  # draw next move
  move_rnd=rand(worm_data.rng,worm_range)
  # cartesian direction
  dir=fld1(move_rnd,2)
  # left or right
  lr=mod1(move_rnd,2)
  # nearest neighbor
  nn=hop(lat.ira,dir,lr,size(lat.angle))

  #weight from the bond, no on-site weight
  if dir==NDIMS
      if lr==1
        #current J
        bond_val=lat.current[lat.ira...,dir]
        #curl of P
        c_val = lat.curl_lattice[lat.ira...,dir]
        effective_bond = bond_val-params.inter*c_val
        chem_weight = disorder.chemical_weight[lat.ira[1],lat.ira[2]]
        #switch worm_data to disorder
        loop_weight = worm_data.weights_inc[effective_bond + MAX_BOND_J + 1]
        # weight = loop_weight*worm_data.weights_self_inc[ bond_val + MAX_BOND_J + 1]
        weight = loop_weight
        weight = weight*chem_weight
        weight_final = min(weight,1.0)

      elseif lr ==2
        bond_val=lat.current[nn...,dir]
        c_val = lat.curl_lattice[nn...,dir]
        effective_bond = bond_val-params.inter*c_val
        chem_weight = disorder.chemical_weight[nn[1],nn[2]]
        loop_weight = worm_data.weights_dec[effective_bond + MAX_BOND_J + 1]
        # weight = loop_weight*worm_data.weights_self_dec[ bond_val + MAX_BOND_J + 1]
        weight = loop_weight
        weight = weight/chem_weight
        weight_final = min(weight,1.0)
      end

      #chemical potential
      #time direction index = NDIMS
  elseif dir==1 ||dir==2
      if lr==1
        bond_val=lat.current[lat.ira...,dir]
        c_val = lat.curl_lattice[lat.ira...,dir]
        effective_bond = bond_val-params.inter*c_val
        weight = disorder.weights_inc[lat.ira[1],lat.ira[2],dir][effective_bond + MAX_BOND_J + 1]
        # weight = weight*worm_data.weights_self_inc[ bond_val + MAX_BOND_J + 1]
        #weight_final = min(weight,1.0)

      elseif lr ==2
        bond_val=lat.current[nn...,dir]
        c_val = lat.curl_lattice[nn...,dir]
        effective_bond = bond_val-params.inter*c_val
        weight = disorder.weights_dec[nn[1],nn[2],dir][effective_bond + MAX_BOND_J + 1]
        # weight = weight*worm_data.weights_self_dec[ bond_val + MAX_BOND_J + 1]
        #weight_final = min(weight,1.0)
      end
  else
      println("Error")
  end

  #ratio of 0.5 due to the 0.5 probability to jump when ira==masha
  if lat.ira==lat.masha
    weight_final=min(weight*2.0,1.0)
  elseif nn==lat.masha
    weight_final=min(weight*0.5,1.0)
  else
    weight_final=min(weight,1.0)
  end

  # accept reject
  accept=(weight_final>rand(worm_data.rng))
  # println(weight,accept)
  if (accept)
    # accept
    # E_before = lat.energy
    # dE = update_energy!(lat,params,disorder,dir,lr,lat.ira)
    lat.energy += -log(weight)
    # prob = min(exp.(-dE),1.0)
    # difference = abs(weight-prob)
    # if (difference > 10.0^(-6.))
    #   println("Probability does not match:", difference, "weight:",weight,"prob:",prob)
    # end


    if lr==1
      #move to the right
      lat.current[lat.ira...,dir]+=1
      lat.winding[dir]+=1
    elseif lr==2
      # move to the left
      lat.current[nn...,dir]-=1
      lat.winding[dir]-=1
    end
    lat.ira=nn

    # E_after = find_energy(lat,params,disorder)
    # if (abs.((E_after-E_before)- dE) > 10.^(-8.))
    #   println("wrong worm update")
    # end


  # else
  end
  return nothing
end

"""
    jump_worm(WormData,Lattice)

jump of worm's head and tail
"""
function jump_worm!(worm_data::WormMoveData,lat::Lattice)
  # new_site=ind2sub(size(lat.angle),rand(worm_data.rng,1:length(lat.angle)))
  new_site = Tuple(CartesianIndices(lat.angle)[rand(worm_data.rng,1:length(lat.angle))])
  if new_site!=lat.ira
    lat.ira=new_site
    lat.masha=new_site
  end
  return nothing
end


end # module
