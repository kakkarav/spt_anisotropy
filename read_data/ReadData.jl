module ReadData

export loadall,loadsome

function FindLongestRun(file)
    line = readlines(file)
    headers = findall(x->x=="----BEGIN PARAMS----",line)

    global start = 1
    global finish = 0
    global progress = 0
    global progress_temp = 0

    if length(headers)==1
        start = 1
        finish = length(line)
        progress  = split(line[finish],",")[1]
    elseif length(headers)>1
        while length(headers)>1
            if headers[2] > headers[1]+10
                finish_temp = headers[2]-1
                temp = split(line[finish_temp],",")[1]
                if temp != "Step"
                    global progress_temp = parse(Int64,temp )
                    if progress_temp > progress
                        global progress = progress_temp
                        global start = headers[1]
                        global finish = finish_temp
                    end
                end
            end
            deleteat!(headers,1)
        end

        if split(line[end],",")[1] != "Step"
            progress_last = split(line[end],",")[1]
            progress_last = parse(Int64,progress_last)
        else
            progress_last = 0
        end

        if progress_last > progress
            global progress = progress_last
            global start = pop!(headers)
            global finish = length(line)
        end
    end
    return longest_line = line[start:finish]
end

function unpack()
    number = 10000
    meas = Any[]

    for n in 0:number
    #for n in 689:689
        if (isfile("$(n)_Best_SPT.out"))
            file = open("$(n)_Best_SPT.out")
            line = FindLongestRun(file)
            #print("test1")
            close(file)
            if length(line) > 20
                test = Dict{String,Any}()
                test["data"] = Any[]
                #We first take out the last element which is our result
                header = "----BEGIN PARAMS----"
                data_header = "Step,Energy,HeatCapacity,RealConductivity,ImagConductivity,StiffLoop,StiffLoopFT,CurrentLoop,CurrentLoopFTRe,CurrentLoopFTIm,StiffVillain,StiffVillainFT,CurrentVillain,CurrentVillainFTRe,CurrentVillainFTIm,Magnitization,CompressibilityL,Green,CurrentVillain"
                while line[end] != data_header
                    result_string = pop!(line)
                    #println(result_string[end] == 1)
                    result = [ ParseData(n) for n in split(result_string,",")]
                    #println(result)
                    push!(test["data"],result)
                end

                filter!(x->x≠"Step,Energy,HeatCapacity,RealConductivity,ImagConductivity,StiffLoop,StiffLoopFT,CurrentLoop,CurrentLoopFTRe,CurrentLoopFTIm,StiffVillain,StiffVillainFT,CurrentVillain,CurrentVillainFTRe,CurrentVillainFTIm,Magnitization,CompressibilityL,Green,CurrentVillain",line)

                for i in line
                    if occursin(",",i)
                        (param,value) = split(i,",")
                        value = parse(Float64,value)
                        test["$param"] = value
                    end
                end
                push!(meas,test)
                #print("test2")
            end
        end
        #println(n)
    end
    println("number of data =",length(meas))
    return meas
end

function loadall(path)
    cd("C:\\Users\\p_5_2\\git\\parse\\data")
    cd(path)
    folder_list = Base.Filesystem.readdir(pwd())
    meas = Any[]
    for folder in folder_list
        cd(folder)
        mea = unpack()
        push!(meas,mea)
        cd("..")
    end
    return meas
end

function ReadArray(array)
    result = [ n =="" ? 0.0 : parse(Float64,n) for n in split(array," ")]
    return result
end

function ParseData(input)
    #println(input,",",typeof(input))
    try
        if occursin(" ",input)
            return ReadArray(input)
        else
            return parse(Float64,input)
        end
    catch
        println("error")
        return "error"
    end
end

function loadsome(folders)
    meas = Any[]
    for folder in folders
        cd(folder)
        mea = unpack()
        push!(meas,mea)
        cd("..")
    end
    return meas
end

end
