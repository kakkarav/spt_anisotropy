module PlotData

export plot_data,StiffL,StiffLFT,StiffV,StiffVFT,plot_thermalize,plot_conductivity,plot_heatcap

using IterTools
using PyPlot
using Statistics
using LsqFit

function foldgreen(green,length)
    fold = Float64[]
    push!(fold,green[1])
    for j in 2:Int(length/2)
        push!(fold,(green[j]+green[Int(length)+2-j])./2)
    end
    push!(fold,green[Int(length./2+1)])
    return fold
end

# function findgap(green,length)
#     @. model(x, p) = p[1]*exp(-x*p[2])+p[3]
#     ydata = foldgreen(green,length)
#     xdata = 1:Int64(length./2+1)
#     p0 = [0.5,0.5,0.0]
#     fit = curve_fit(model, xdata, ydata, p0)
#     return coef(fit)[2],stderror(fit)[2]
# end

# function findgap(green,length)
#     @. model(x, p) = p[1]*exp(-x*p[2])/x^(p[3])
#     ydata = foldgreen(green,length)
#     xdata = 1:Int64(length./2+1)
#     p0 = [0.5,0.5,1.0]
#     fit = curve_fit(model, xdata, ydata, p0)
#     return coef(fit)[2],stderror(fit)[2]
# end

# function findgap(green,length)
#     @. model(x, p) = p[1]*exp(-x*p[2])
#     ydata = foldgreen(green,length)
#     xdata = 1:Int64(length./2+1)
#     p0 = [0.5,0.5]
#     fit = curve_fit(model, xdata, ydata, p0)
#     return coef(fit)[2],stderror(fit)[2]
# end

function findgap(green,L,p0)
    @. model(x, p) = p[1]*(exp(-x*p[2])+exp(-(L-x)*p[2]))
    # @. model(x, p) = p[1]*(x^p[2]+(L-x)^p[2])
    ydata = green[2:end]
    xdata = 1:(L-1)
    # p0 = [0.5,0.5]
    fit = curve_fit(model, xdata, ydata, p0)
    # fig,ax=PyPlot.subplots()
    # plot(xdata,ydata,label="real")
    # plot(xdata,model(xdata,coef(fit)),label="fitting")
    # title(coef(fit)[2])
    # grid(true)
    # ax.legend(loc="best", fontsize=10)
    return coef(fit)[2],stderror(fit)[2]
end
#the x-axis"
function plot_data(all_meas,element,data,scale,path,description)
    list = ["Completeness","Energy","Heat Capacity","Re Conductivity","Im Conductivity",
        "Stiff Loop","Stiff FT Loop","Current Loop","Re Current Loop","Im Current Loop",
        "Stiff villain","Stiff FT Villain","Current Time Villain","Re Current FT Villain","Im Current FT villain",
        "Magnitization","Compressibility Loop","Gap Loop","Normalized gap loop variance","Histogram Loop",
        "Compressibility Villain","Gap Villain","Error Green Villain","Conductviity Loop","ZRatio",
        "Order parameter loop","Order parameter Villian"]

    xlabel_name = Dict([("chemical_disorder", "W (Chemical potential disorder)"), ("lambda1", "\$ \\lambda_2 \$"), ("lambda2", "\$ \\lambda_1 \$")])
    ylabel_name = ["Completeness","Action (S) ","Heat Capacity","Re Conductivity","Im Conductivity",
        "\$ \\rho_1(k=0,\\omega=0) \$","\$ \\rho_1(k=0,\\omega=2 \\pi/L) \$","\$ J_{x 1} \$","Re Current Loop","Im Current Loop",
        "\$ \\rho_2(k=0,\\omega=0) \$","\$ \\rho_2(k=0,\\omega=2 \\pi/L) \$","\$ J_{\\tau 2} \$","Re Current FT Villain","Im Current FT villain",
        "\$ \\Sigma \\exp^{i\\theta(r,\\tau)} \$","\$ \\kappa_1 \$","\$ \\Delta_1 (\\Delta \\tau = \\lambda/t = 1/ U \\lambda) \$","Normalized gap loop variance","Histogram Loop",
        "\$ \\kappa_2 \$","\$ \\Delta_2 (\\Delta \\tau = \\lambda/t = 1/ U \\lambda) \$","Error Green Villain","Conductviity Loop","ZRatio",
        "Order parameter loop","Order parameter Villian"]

    if !(data in [5,7,11,12,17,18,19,20,21,22,23,24,25,26,27]) #plot for Float64

        fig,ax=PyPlot.subplots()
        for meas in all_meas
            sort!(meas,by=x->x["L"])
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = []

            for i in groupby(x -> x["L"],meas)
                c_L=i[1]["L"]
                cond = []
                test= []
                point =[]
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    conductivity = [ r["data"][1][data]*(c_L^(scale)) for r in j]
                    push!(cond,mean(conductivity))
                    push!(test,std(conductivity)/sqrt(length(j)))
                    append!(point,length(j))
                end
                push!(points,point)
            #     ax[:plot](disorder,cond,label=c_L)
                # println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                errorbar(axis[1:length(cond)],cond,yerr=test,label=c_L)
            end
        end
        println(string("done:",data))
        ax.legend(loc="best", fontsize=10)
        xlabel(xlabel_name[element], fontsize =15)
        ylabel(ylabel_name[data], fontsize = 15)
        # title(string(list[data],", scale=",scale))
        title(string(list[data],", scale=",scale,", ",description))
        grid(true)
        fig.savefig(string(path,list[data]))
    # else
    #     return nothing

    elseif (data==5)#imaginary conpressibility
        fig,ax=PyPlot.subplots()
        for meas in all_meas
            sort!(meas,by=x->x["L"])
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = []

            for i in groupby(x -> x["L"],meas)
                c_L=i[1]["L"]
                cond = []
                test= []
                point =[]
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    conductivity = [ r["data"][1][data]*(c_L^(scale)) for r in j]
                    push!(cond,mean(conductivity))
                    push!(test,std(conductivity)/sqrt(length(j)))
                    append!(point,length(j))
                end
                push!(points,point)
            #     ax[:plot](disorder,cond,label=c_L)
                # println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                errorbar(axis[1:length(cond)],cond,yerr=test,label=c_L)
            end
        end
        println(string("done:",data))
        ax.set_ylim([0,1.0])
        ax.legend(loc="best", fontsize=10)
        xlabel(xlabel_name[element], fontsize =15)
        ylabel(ylabel_name[data], fontsize = 15)
        # title(string(list[data],", scale=",scale))
        title(string(list[data],", scale=",scale,", ",description))
        grid(true)
        fig.savefig(string(path,list[data]))


    elseif (data==7) #plot for arrays
        fig,ax=PyPlot.subplots()
        for meas in all_meas
            sort!(meas,by=x->x["L"])
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = []

            for i in groupby(x -> x["L"],meas)
                c_L=i[1]["L"]
                cond = []
                test= []
                point =[]
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    conductivity = [ (r["data"][1][data][1] .+r["data"][1][9][1].^2. .+r["data"][1][10][1].^2)*(c_L^(scale))  for r in j]
                    push!(cond,mean(conductivity))
                    push!(test,std(conductivity)/sqrt(length(j)))
                    append!(point,length(j))
                end
                push!(points,point)
            #     ax[:plot](disorder,cond,label=c_L)
                # println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                errorbar(axis[1:length(cond)],cond,yerr=test,label=c_L)
            end
            # println(points)
        end
        println(string("done:",data))
        ax.legend(loc="best", fontsize=10)
        xlabel(xlabel_name[element], fontsize =15)
        ylabel(ylabel_name[data], fontsize = 15)
        title(string(list[data],", scale=",scale,", ",description))
        grid(true)
        fig.savefig(string(path,"StiffnessFTLoop"))

    elseif (data==11) #plot for arrays
        fig,ax=PyPlot.subplots()
        for meas in all_meas
            sort!(meas,by=x->x["L"])
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = []

            for i in groupby(x -> x["L"],meas)
                c_L=i[1]["L"]
                cond = []
                test= []
                point =[]
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    conductivity = [ (r["data"][1][data][1] .+r["data"][1][23][1].^2)*(c_L^(scale)) for r in j]
                    push!(cond,mean(conductivity))
                    push!(test,std(conductivity)/sqrt(length(j)))
                    append!(point,length(j))
                end
                push!(points,point)
            #     ax[:plot](disorder,cond,label=c_L)
                # println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                # println(cond)
                errorbar(axis[1:length(cond)],cond,yerr=test,label=c_L)
            end
            # println(points)
        end
        println(string("done:",data))
        ax.legend(loc="best", fontsize=10)
        xlabel(xlabel_name[element], fontsize =15)
        ylabel(ylabel_name[data], fontsize = 15)
        # title(string(list[data],", scale=",scale))
        title(string(list[data],", scale=",scale,", ",description))
        grid(true)
        fig.savefig(string(path,list[data]))

    elseif (data==12) #plot for arrays
        fig,ax=PyPlot.subplots()
        for meas in all_meas
            sort!(meas,by=x->x["L"])
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = []

            for i in groupby(x -> x["L"],meas)
                c_L=i[1]["L"]
                cond = []
                test= []
                point =[]
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    conductivity = [ (r["data"][1][data][1]  .+r["data"][1][14][1].^2 .+r["data"][1][15][1].^2)*(c_L^(scale)) for r in j]
                    push!(cond,mean(conductivity))
                    push!(test,std(conductivity)/sqrt(length(j)))
                    append!(point,length(j))
                end
                push!(points,point)
            #     ax[:plot](disorder,cond,label=c_L)
                # println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                # println(cond)
                errorbar(axis[1:length(cond)],cond,yerr=test,label=c_L)
            end
            # println(points)
        end
        println(string("done:",data))
        ax.legend(loc="best", fontsize=10)
        xlabel(xlabel_name[element], fontsize =15)
        ylabel(ylabel_name[data], fontsize = 15)
        # title(string(list[data],", scale=",scale))
        title(string(list[data],", scale=",scale,", ",description))
        grid(true)
        fig.savefig(string(path,list[data]))


    # elseif (data==18) #plot for arrays
    #
    #     fig,ax=PyPlot.subplots()
    #     for meas in all_meas
    #         sort!(meas,by=x->x["L"]) #sort all element by site
    #         axis =[ sample[element] for sample in meas]
    #         axis = sort(unique(axis))
    #
    #         points = [] #the number of data for each point
    #
    #         for i in groupby(x -> x["L"],meas) #now we group them by size
    #             c_L=i[1]["L"]
    #             cond = [] #the value for each points
    #             test= [] #the error for each point from statistics
    #             point =[] #the number of data for each point
    #             sort!(i,by=x->x[element])
    #             for j in groupby(x->x[element],i)
    #                 conductivity = [ r["data"][1][data]*(c_L^(scale)) for r in j]
    #                 avg_green = mean(conductivity)
    #                 gap,error = findgap(avg_green,c_L)
    #                 push!(cond,gap)
    #                 push!(test,error)
    #                 append!(point,length(j))
    #             end
    #             push!(points,point)
    #         #     ax[:plot](disorder,cond,label=c_L)
    #             println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
    #             errorbar(axis[1:length(cond)],cond,yerr=test,label=c_L)
    #         end
    #         println(points)
    #     end
    #     ax.legend(loc="best", fontsize=10)
    #     xlabel(xlabel_name[element], fontsize =15)
    #     title(string(list[data],", scale=",scale))
    #     grid(true)
elseif (data==17)#compressibility loop
        fig,ax=PyPlot.subplots()
        for meas in all_meas
            sort!(meas,by=x->x["L"])
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = []

            for i in groupby(x -> x["L"],meas)
                c_L=i[1]["L"]
                cond = []
                test= []
                point =[]
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    conductivity = [ (r["data"][1][17] - r["data"][1][8].^2.0)*(c_L^(scale)) for r in j]
                    push!(cond,mean(conductivity))
                    push!(test,std(conductivity)/sqrt(length(j)))
                    append!(point,length(j))
                end
                push!(points,point)
            #     ax[:plot](disorder,cond,label=c_L)
                # println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                errorbar(axis[1:length(cond)],cond,yerr=test,label=c_L)
            end
        end
        println(string("done:",data))
        ax.legend(loc="best", fontsize=10)
        xlabel(xlabel_name[element], fontsize =15)
        ylabel(ylabel_name[data], fontsize = 15)
        title(string(list[data],", scale=",scale,", ",description))
        grid(true)
        fig.savefig(string(path,list[data]))

    elseif (data==18) #plot for gaps of loop

        fig,ax=PyPlot.subplots()
        for meas in all_meas
            sort!(meas,by=x->x["L"]) #sort all element by site
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = [] #the number of data for each point

            for i in groupby(x -> x["L"],meas) #now we group them by size
                c_L=i[1]["L"]
                cond = [] #the value for each points
                test= [] #the error for each point from statistics
                point =[] #the number of data for each point
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    conductivity = [ r["data"][1][18]/r["data"][1][22]*(c_L^(scale)) for r in j]
                    avg_green = mean(conductivity)
                    # fig,ax=PyPlot.subplots()
                    # plot(avg_green)
                    p0 = [avg_green[1],1.0]
                    gap,error = findgap(avg_green,c_L,p0)
                    push!(cond,gap)
                    push!(test,error)
                    append!(point,length(j))
                end
                push!(points,point)
            #     ax[:plot](disorder,cond,label=c_L)
                # println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                errorbar(axis[1:length(cond)],cond,yerr=test,label=c_L)
            end
            # println(points)
        end
        println(string("done:",data))
        ax.legend(loc="best", fontsize=10)
        xlabel(xlabel_name[element], fontsize =15)
        ylabel(ylabel_name[data], fontsize = 15)
        title(string(list[data],", scale=",scale,", ",description))
        grid(true)
        fig.savefig(string(path,"Gap Loop"))

    elseif (data==19) #gap variance

        fig,ax=PyPlot.subplots()
        for meas in all_meas
            sort!(meas,by=x->x["L"]) #sort all element by site
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = [] #the number of data for each point

            for i in groupby(x -> x["L"],meas) #now we group them by size
                c_L=i[1]["L"]
                cond = []
                test= []
                point =[] #the number of data for each point
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    conductivity = [ findgap(r["data"][1][18],c_L)[1]*(c_L^(scale)) for r in j]
                    push!(cond,mean(conductivity))
                    push!(test,std(conductivity))
                    append!(point,length(j))
                end
                push!(points,point)
            #     ax[:plot](disorder,cond,label=c_L)
                # println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                plot(axis[1:length(cond)],test ./cond,label=c_L)
            end
            # println(points)
        end
        println(string("done:",data))
        ax.legend(loc="best", fontsize=10)
        xlabel(xlabel_name[element], fontsize =15)
        title(string(list[data],", scale=",scale))
        grid(true)

    elseif (data==20) #gap histogram

        for meas in all_meas
            sort!(meas,by=x->x["L"]) #sort all element by site
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = [] #the number of data for each point

            for i in groupby(x -> x["L"],meas) #now we group them by size
                c_L=i[1]["L"]
                point =[] #the number of data for each point
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    fig,ax=PyPlot.subplots()
                    coupling = j[1]["lambda1"]
                    conductivity = [ findgap(r["data"][1][18],c_L)[2]*(c_L^(scale)) for r in j]
                    # println(sort(conductivity))
                    append!(point,length(j))
                    hist(conductivity,10)
                    title(string(list[data],", L=",c_L,", lambda =",coupling))
                    ax.set_xlim(left=0, right=0.005)
                    grid(true)
                end
                push!(points,point)
            end
            # println(points)
        end
        println(string("done:",data))

    elseif (data==21)#compressibility Villain
            fig,ax=PyPlot.subplots()
            for meas in all_meas
                sort!(meas,by=x->x["L"])
                axis =[ sample[element] for sample in meas]
                axis = sort(unique(axis))

                points = []

                for i in groupby(x -> x["L"],meas)
                    c_L=i[1]["L"]
                    cond = []
                    test= []
                    point =[]
                    sort!(i,by=x->x[element])
                    for j in groupby(x->x[element],i)
                        conductivity = [ (r["data"][1][21] - r["data"][1][13].^2.0)*(c_L^(scale)) for r in j]
                        push!(cond,mean(conductivity))
                        push!(test,std(conductivity)/sqrt(length(j)))
                        append!(point,length(j))
                    end
                    push!(points,point)
                #     ax[:plot](disorder,cond,label=c_L)
                    # println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                    errorbar(axis[1:length(cond)],cond,yerr=test,label=c_L)
                end
            end
            println(string("done:",data))
            ax.legend(loc="best", fontsize=10)
            xlabel(xlabel_name[element], fontsize =15)
            ylabel(ylabel_name[data], fontsize = 15)
            title(string(list[data],", scale=",scale,", ",description))
            grid(true)
            fig.savefig(string(path,list[data]))

    elseif (data==22) #plot for gaps of villain

        fig,ax=PyPlot.subplots()
        for meas in all_meas
            sort!(meas,by=x->x["L"]) #sort all element by site
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = [] #the number of data for each point

            for i in groupby(x -> x["L"],meas) #now we group them by size
                c_L=i[1]["L"]
                cond = [] #the value for each points
                test= [] #the error for each point from statistics
                point =[] #the number of data for each point
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    conductivity = [ r["data"][1][19]*(c_L^(scale)) for r in j]
                    avg_green = mean(conductivity)
                    p0 = [avg_green[1],1.0]
                    # fig,ax=PyPlot.subplots()
                    # plot(avg_green)
                    gap,error = findgap(avg_green,c_L,p0)
                    # println(gap, ",", error)
                    push!(cond,gap)
                    push!(test,error)
                    append!(point,length(j))
                end
                # print("Done")
                push!(points,point)
            #     ax[:plot](disorder,cond,label=c_L)
                # println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                # errorbar(axis[1:length(cond)],cond,yerr=test,label=c_L)
                plot(axis[1:length(cond)],cond,label=c_L)
            end
            # println(points)
        end
        println(string("done:",data))
        ax.legend(loc="best", fontsize=10)
        xlabel(xlabel_name[element], fontsize =15)
        ylabel(ylabel_name[data], fontsize = 15)
        title(string(list[data],", scale=",scale,", ",description))
        grid(true)
        fig.savefig(string(path,"Gap Villain"))


    elseif (data==23) #plot for error of green function of Villain
        for meas in all_meas
            sort!(meas,by=x->x["L"]) #sort all element by site
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = [] #the number of data for each point

            for i in groupby(x -> x["L"],meas) #now we group them by size
                fig,ax=PyPlot.subplots()
                c_L=i[1]["L"]
                momentum = [2*m*pi./c_L for m in 1:(c_L-1) ]
                # cond = Array{Float64,1}[] #the value for each points
                # test= Array{Float64,1}[] #the error for each point from statistics
                point =[] #the number of data for each point
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    coupling = j[1]["lambda1"]
                    conductivity = [ r["data"][1][19] for r in j]
                    conductivity_err = [ r["data"][1][20]*(c_L^(scale)) for r in j]
                    error = conductivity_err ./conductivity
                    # append!(point,length(j))
                    # println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                    # errorbar(1:c_L,cond,yerr=test,label=coupling)
                    plot(1:c_L,cond,label=coupling)
                    # println(log.(momentum))
                end
                push!(points,point)
                ax.legend(loc="best", fontsize=8)
                ax.set_xlim(left=0, right=2*pi);
                xlabel("Imaginary frequency")
                title(string(list[data],", L=",c_L))
                grid(true)
                # print(c_L)
                fig.savefig(string(path,"conductivity",c_L,".png"))
            end
            # println(points)
            println(string("done:",data))
        end

    elseif (data==24) #plot for current-current correltion function
        for meas in all_meas
            sort!(meas,by=x->x["L"]) #sort all element by site
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = [] #the number of data for each point

            for i in groupby(x -> x["L"],meas) #now we group them by size
                fig,ax=PyPlot.subplots()
                c_L=i[1]["L"]
                momentum = [2*m*pi./c_L for m in 1:(c_L-1) ]
                # cond = Array{Float64,1}[] #the value for each points
                # test= Array{Float64,1}[] #the error for each point from statistics
                point =[] #the number of data for each point
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    coupling = j[1]["lambda1"]
                    # conductivity = [ (r["data"][1][7] .-r["data"][1][6])*(c_L^(scale)) for r in j]
                    conductivity = [ (r["data"][1][7])*(c_L^(scale)) for r in j]
                    cond = mean(conductivity)
                    # cond = cond ./momentum
                    # cond = (mean(conductivity)./momentum)
                    cond = (mean(conductivity)./momentum)
                    test = std(conductivity)/sqrt(length(j))
                    append!(point,length(j))
                    # println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                    errorbar(momentum,cond,yerr=test,label=coupling)
                    # println(log.(momentum))
                end
                push!(points,point)
                ax.legend(loc="best", fontsize=8)
                ax.set_xlim(left=0, right=2*pi);
                xlabel("Imaginary frequency")
                title(string(list[data],", L=",c_L))
                grid(true)
                # print(c_L)
                fig.savefig(string(path,"conductivity",c_L,".png"))
            end
            # println(points)
            println(string("done:",data))
        end

    elseif (data==25) #ZRatio loop
        fig,ax=PyPlot.subplots()
        for meas in all_meas
            sort!(meas,by=x->x["L"])
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = []

            for i in groupby(x -> x["L"],meas)
                c_L=i[1]["L"]
                cond = []
                test= []
                point =[]
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    conductivity = [ r["data"][1][22]*(c_L^(scale)) for r in j]
                    push!(cond,mean(conductivity))
                    push!(test,std(conductivity)/sqrt(length(j)))
                    append!(point,length(j))
                end
                push!(points,point)
            #     ax[:plot](disorder,cond,label=c_L)
                # println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                errorbar(axis[1:length(cond)],cond,yerr=test,label=c_L)
            end
        end
        println(string("done:",data))
        ax.legend(loc="best", fontsize=10)
        xlabel(xlabel_name[element], fontsize =15)
        title(string(list[data],", scale=",scale))
        grid(true)
        fig.savefig(string(path,list[data]))

    elseif (data==26) #Order Parameter loop
        fig,ax=PyPlot.subplots()
        for meas in all_meas
            sort!(meas,by=x->x["L"]) #sort all element by site
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = [] #the number of data for each point

            for i in groupby(x -> x["L"],meas) #now we group them by size
                c_L=i[1]["L"]
                cond = [] #the value for each points
                test= [] #the error for each point from statistics
                point =[] #the number of data for each point
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    conductivity = [ r["data"][1][18][1]/r["data"][1][22]*(c_L^(scale)) for r in j]
                    push!(cond,mean(conductivity))
                    push!(test,std(conductivity)/sqrt(length(conductivity)))
                    append!(point,length(j))
                end
                push!(points,point)
            #     ax[:plot](disorder,cond,label=c_L)
                # println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                errorbar(axis[1:length(cond)],cond,yerr=test,label=c_L)
            end
            # println(points)
        end
        println(string("done:",data))
        ax.legend(loc="best", fontsize=10)
        xlabel(xlabel_name[element], fontsize =15)
        title(string(list[data],", scale=",scale))
        grid(true)
        fig.savefig(string(path,list[data]))

    elseif (data==27) #Order Parameter Villain
        fig,ax=PyPlot.subplots()
        for meas in all_meas
            sort!(meas,by=x->x["L"]) #sort all element by site
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = [] #the number of data for each point

            for i in groupby(x -> x["L"],meas) #now we group them by size
                c_L=i[1]["L"]
                cond = [] #the value for each points
                test= [] #the error for each point from statistics
                point =[] #the number of data for each point
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    conductivity = [ r["data"][1][19][1]*(c_L^(scale)) for r in j]
                    push!(cond,mean(conductivity))
                    push!(test,std(conductivity)/sqrt(length(conductivity)))
                    append!(point,length(j))
                end
                push!(points,point)
            #     ax[:plot](disorder,cond,label=c_L)
                # println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                errorbar(axis[1:length(cond)],cond,yerr=test,label=c_L)
            end
            # println(points)
        end
        println(string("done:",data))
        ax.legend(loc="best", fontsize=10)
        xlabel(xlabel_name[element], fontsize =15)
        title(string(list[data],", scale=",(scale+2)))
        grid(true)
        fig.savefig(string(path,list[data]))
    end

end

function plot_thermalize(meas,element,data,scale)
    list = ["Step","Energy","Heat Capacity","Re Conductivity","Im Conductivity","Stiff Loop",
        "Stiff FT Loop","Current Loop","Re Current Loop","Im Current Loop","Stiff villain",
        "Stiff FT Villain","Current Villain","Re Current FT Villain","Im Current FT villain","Magnitization"]
    sort!(meas,by=x->x["L"])
    axis =[ sample[element] for sample in meas]
    axis = sort(unique(axis))

    points = []

    for i in groupby(x -> x["L"],meas)
        c_L=i[1]["L"]
        cond = []
        test= []
        point =[]
        sort!(i,by=x->x[element])
        for j in groupby(x->x[element],i)
            fig,ax=PyPlot.subplots()
            for realization in j
                percentage = [ realization["data"][time][1] for time in 1:length(realization["data"])]
                conductivity = [ realization["data"][time][data]*(c_L^(scale)) for time in  1:length(realization["data"])]
                ax[:plot](percentage,conductivity)
            end
            xlabel("Percentage")
            name = string(element, "= $(j[1][element])")
            title(name)
            grid(true)
        end
    end
end

function plot_conductivity(all_meas,element,scale,fit,path)

    @. model1(x, p) = p[1]*(x)^p[2]
    p0 = [1.0,1.0]
    lb = [0.0,0.0]

    @. model2(x, p) = p[1]*(x-p[2])^(1/2)
    p0 = [1.0,0.0]
    lb = [0.0,0.0]

    # @. model(x, p) = p[1]*log.(x-p[2]) + p[3]
    # p0 = [1.0,0.0,0.0]
    # lb = [0.0,0.0,-100.0]


    # data = []
    # for i in all_meas
    #     append!(data,i)
    # end
    for data in all_meas
        sort!(data,by=x->x[element])


        for i in groupby(x -> x[element],data) #now we group them by size
            fig,ax=PyPlot.subplots()
            coupling = i[1][element]
            if coupling < 1.0
                point =[] #the number of data for each point
                LS =[ sample["L"] for sample in i]
                LS = sort(unique(LS))
                xdata = map(x-> 2*pi/x,LS)
                ydata = Float64[]
                sort!(i,by=x->x["L"])
                for j in groupby(x->x["L"],i)
                    c_L=i[1]["L"]
                    # conductivity = [ (r["data"][1][7][1] .-r["data"][1][6])*(c_L^(scale)) for r in j]
                    conductivity = [ (r["data"][1][7][1])*(c_L^(scale)) for r in j]
                    cond = mean(conductivity)*c_L/(2*pi)
                    append!(ydata,cond)
                end
                if fit
                    fit1 = curve_fit(model1, xdata, ydata, p0,lower=lb)
                    # fit2 = curve_fit(model2, xdata, ydata, p0,lower=lb)
                    # ydata = log.(ydata)
                    plot(xdata,ydata,label="real")
                    plot(xdata,model1(xdata,coef(fit1)),label="fitting quadratic")
                    # plot(xdata,model2(xdata,coef(fit2)),label="fitting gap")
                    ax.set_xlim(left=0, right=1.0)
                    ax.set_ylim(bottom=0)
                    # title(string("lambda =", coupling, ", coefs =", coef(fit1), " ",coef(fit2)))
                    title(string(element," =", coupling, ", coefs =", coef(fit1)))
                    # title(string("lambda =", coupling))
                    grid(true)
                    ax.legend(loc="best", fontsize=10)
                    # println(coupling)
                else
                    plot(xdata,ydata,label="real")
                    ax.set_xlim(left=0, right=1.0)
                    ax.set_ylim(bottom=0)
                    title(string(element," =", coupling))
                    grid(true)
                    # println(coupling)
                end
            end
            fig.savefig(string(path,"condfit ",element," ",coupling,".png"))
        end
    end
end


function plot_heatcap(all_meas,element,scale,path)
    for meas in all_meas
        sort!(meas,by=x->x["L"]) #sort all element by site
        axis =[ sample[element] for sample in meas]
        axis = sort(unique(axis))

        points = [] #the number of data for each point

        for i in groupby(x -> x["L"],meas) #now we group them by size
            c_L=i[1]["L"]
            point =[] #the number of data for each point
            sort!(i,by=x->x[element])
            for j in groupby(x->x[element],i)
                fig,ax=PyPlot.subplots()
                coupling = j[1]["lambda1"]
                conductivity = [ r["data"][1][3]*(c_L^(scale)) for r in j]
                # println(sort(conductivity))
                append!(point,length(j))
                hist(conductivity,10)
                title(string("Heat cap @ L=",c_L,", lambda =",coupling))
                ax.legend(loc="best", fontsize=10)
                grid(true)
                fig.savefig(string(path,string("opt_cond_lambda",coupling)))
            end
            push!(points,point)
        end
        # println(points)
    end
end


end
