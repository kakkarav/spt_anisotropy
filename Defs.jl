module Defs

export NDIMS,cyclic,MAX_BOND_J,odd,binding_range,worm_range

const NDIMS=3
#a cyclic list used for the curl function
const cyclic = [1 2 3 1 2 3]
#max bond value
const MAX_BOND_J=500 #current variable
#a matrix that its elements return a number that is missing from a list 123, e.g. odd[1,2] =3, odd[2,3] = 1
#this will be used for updating the curl-P lattice
const odd = [0 3 2; 3 0 1; 2 1 0]

const binding_range = 1:9
const worm_range = 1:(2*NDIMS)

end
