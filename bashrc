#!/bin/bash

export PS1="[\u@\h:\w]\n$ "
module unload intel
module load gcc/7.1.0-fasrc01
module load julia/1.0.0-fasrc01
module load python/3.6.3-fasrc02


#export PS1="\[\033[01;34m\][\u\[\033[00m\]\[\033[01;31m\]@\h\[\033[00m\] \[\033[01;34m\]\W \t]\[\033[00m\]\[\033[01;37m\]\$\[\033[00m\] "
alias bestwork='cd /n/lukin_lab2/Users/nyao/Best/git/spt_debug/0.7_green_villain'
alias bestoutput='cd /n/lukin_lab2/Users/nyao/Best/output'
alias besttest='srun --pty -p test -t 0-3:00 --mem 2000 /bin/bash'
alias queue='squeue -u nyao'
alias bestqueue='squeue -u nyao | grep "$1"'

