module readdata



export plot_data

using IterTools
using PyPlot
using Statistics
using LsqFit

function plot_data(all_meas,element,data,scale)
    list = ["Step","Energy","Energy fluctuation (heat capacity)","Re Conductivity","sigma_xy (Im Conductivity)","Stiffness Loop",
        "FT Stiffness Loop","Current Loop","Re Current Loop","Im Current Loop","Stiffness villain",
        "FT Stiff Villain","Current Villain","Re Current FT Villain","Im Current FT villain","Magnitization",
        "Compressibility","Gap","Normalized gap variance","Gap distribution","Conductivity Loop"]

    if !(data in [7,18,19,20,21]) #plot for Float64

        fig,ax=PyPlot.subplots()
        for meas in all_meas
            sort!(meas,by=x->x["L"])
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = []

            for i in groupby(x -> x["L"],meas)
                c_L=i[1]["L"]
                cond = []
                test= []
                point =[]
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    conductivity = [ r["data"][1][data]*(c_L^(scale)) for r in j]
                    push!(cond,mean(conductivity))
                    push!(test,std(conductivity)/sqrt(length(j)))
                    append!(point,length(j))
                end
                push!(points,point)
            #     ax[:plot](disorder,cond,label=c_L)
                println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                errorbar(axis[1:length(cond)],cond,yerr=test,label=c_L)
            end
            println(points)
        end
        ax.legend(loc="best", fontsize=10)
        xlabel(element)
        title(list[data])
        grid("on")
    # else
    #     return nothing
    elseif (data==7) #plot for arrays
        fig,ax=PyPlot.subplots()
        for meas in all_meas
            sort!(meas,by=x->x["L"])
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = []

            for i in groupby(x -> x["L"],meas)
                c_L=i[1]["L"]
                cond = []
                test= []
                point =[]
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    conductivity = [ r["data"][1][data][1]*(c_L^(scale)) for r in j]
                    push!(cond,mean(conductivity))
                    push!(test,std(conductivity)/sqrt(length(j)))
                    append!(point,length(j))
                end
                push!(points,point)
            #     ax[:plot](disorder,cond,label=c_L)
                println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                errorbar(axis[1:length(cond)],cond,yerr=test,label=c_L)
            end
            println(points)
        end
        ax.legend(loc="best", fontsize=10)
        xlabel(element)
        title(list[data])
        grid("on")


    # elseif (data==18) #plot for arrays
    #
    #     fig,ax=PyPlot.subplots()
    #     for meas in all_meas
    #         sort!(meas,by=x->x["L"]) #sort all element by site
    #         axis =[ sample[element] for sample in meas]
    #         axis = sort(unique(axis))
    #
    #         points = [] #the number of data for each point
    #
    #         for i in groupby(x -> x["L"],meas) #now we group them by size
    #             c_L=i[1]["L"]
    #             cond = [] #the value for each points
    #             test= [] #the error for each point from statistics
    #             point =[] #the number of data for each point
    #             sort!(i,by=x->x[element])
    #             for j in groupby(x->x[element],i)
    #                 conductivity = [ r["data"][1][data]*(c_L^(scale)) for r in j]
    #                 avg_green = mean(conductivity)
    #                 gap,error = findgap(avg_green,c_L)
    #                 push!(cond,gap)
    #                 push!(test,error)
    #                 append!(point,length(j))
    #             end
    #             push!(points,point)
    #         #     ax[:plot](disorder,cond,label=c_L)
    #             println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
    #             errorbar(axis[1:length(cond)],cond,yerr=test,label=c_L)
    #         end
    #         println(points)
    #     end
    #     ax.legend(loc="best", fontsize=10)
    #     xlabel(element)
    #     title(list[data])
    #     grid("on")
elseif (data==18) #plot for gaps

    fig,ax=PyPlot.subplots()
    for meas in all_meas
        sort!(meas,by=x->x["L"]) #sort all element by site
        axis =[ sample[element] for sample in meas]
        axis = sort(unique(axis))

        points = [] #the number of data for each point

        for i in groupby(x -> x["L"],meas) #now we group them by size
            c_L=i[1]["L"]
            cond = [] #the value for each points
            test= [] #the error for each point from statistics
            point =[] #the number of data for each point
            sort!(i,by=x->x[element])
            for j in groupby(x->x[element],i)
                conductivity = [ r["data"][1][data]*(c_L^(scale)) for r in j]
                avg_green = mean(conductivity)
                gap,error = findgap(avg_green,c_L)
                push!(cond,gap)
                push!(test,error)
                append!(point,length(j))
            end
            push!(points,point)
        #     ax[:plot](disorder,cond,label=c_L)
            println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
            errorbar(axis[1:length(cond)],cond,yerr=test,label=c_L)
        end
        println(points)
    end
    ax.legend(loc="best", fontsize=10)
    xlabel(element)
    title(list[data])
    grid("on")

    elseif (data==19) #gap variance

        fig,ax=PyPlot.subplots()
        for meas in all_meas
            sort!(meas,by=x->x["L"]) #sort all element by site
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = [] #the number of data for each point

            for i in groupby(x -> x["L"],meas) #now we group them by size
                c_L=i[1]["L"]
                cond = []
                test= []
                point =[] #the number of data for each point
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    conductivity = [ findgap(r["data"][1][18],c_L)[1]*(c_L^(scale)) for r in j]
                    push!(cond,mean(conductivity))
                    push!(test,std(conductivity))
                    append!(point,length(j))
                end
                push!(points,point)
            #     ax[:plot](disorder,cond,label=c_L)
                println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                plot(axis[1:length(cond)],test ./cond,label=c_L)
            end
            println(points)
        end
        ax.legend(loc="best", fontsize=10)
        xlabel(element)
        title(list[data])
        grid("on")

    elseif (data==20) #gap histogram

        for meas in all_meas
            sort!(meas,by=x->x["L"]) #sort all element by site
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = [] #the number of data for each point

            for i in groupby(x -> x["L"],meas) #now we group them by size
                c_L=i[1]["L"]
                point =[] #the number of data for each point
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    fig,ax=PyPlot.subplots()
                    coupling = j[1]["lambda1"]
                    conductivity = [ findgap(r["data"][1][18],c_L)[2]*(c_L^(scale)) for r in j]
                    # println(sort(conductivity))
                    append!(point,length(j))
                    hist(conductivity,10)
                    title(string(list[data],", L=",c_L,", lambda =",coupling))
                    ax.set_xlim(left=0, right=0.005)
                    grid("on")
                end
                push!(points,point)
            end
            println(points)
        end
    elseif (data==21) #plot for current-current correltion function
        for meas in all_meas
            sort!(meas,by=x->x["L"]) #sort all element by site
            axis =[ sample[element] for sample in meas]
            axis = sort(unique(axis))

            points = [] #the number of data for each point

            for i in groupby(x -> x["L"],meas) #now we group them by size
                fig,ax=PyPlot.subplots()
                c_L=i[1]["L"]
                momentum = [2*m*pi./c_L for m in 1:(c_L-1) ]
                # cond = Array{Float64,1}[] #the value for each points
                # test= Array{Float64,1}[] #the error for each point from statistics
                point =[] #the number of data for each point
                sort!(i,by=x->x[element])
                for j in groupby(x->x[element],i)
                    coupling = j[1]["lambda1"]
                    conductivity = [ (r["data"][1][7] .-r["data"][1][6])*(c_L^(scale)) for r in j]
                    cond = mean(conductivity)
                    # cond = cond ./momentum
                    cond = (mean(conductivity)./momentum)
                    test = std(conductivity)/sqrt(length(j))
                    append!(point,length(j))
                    println("L=",c_L,"   length data=", length(cond),"  length error=",length(test))
                    errorbar(momentum,cond,yerr=test,label=coupling)
                    # println(log.(momentum))
                end
                push!(points,point)
                ax.legend(loc="best", fontsize=8)
                ax[:set_xlim](xmin=0, xmax=2*pi);
                xlabel("Imaginary frequency")
                title(string(list[data],", L=",c_L))
                grid("on")
            end
            println(points)
        end
    end
end

end 
