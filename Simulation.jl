module Simulation

export Sim

using ..Defs
using ..SimParameters
using ..LatticeData
using ..Update_angle
using ..Update_J
using ..Update_p
using ..Update_fluctuate
using ..Update_binding
using ..Measurements
using ..DisorderData
#using ..Visualize

import RandomNumbers
import RandomNumbers.MersenneTwisters


struct Sim
  lat::Lattice
  worm_data::WormMoveData
  sim_params::SimParams
  measurements::MeasurementArray
  rng::RandomNumbers.MersenneTwisters.MT19937
  fourier::Array{Array{Complex{Float64},1},1}
  disorder::Disorder
  function Sim(sim_params::SimParams)
    rng=RandomNumbers.MersenneTwisters.MT19937(sim_params.seed)
    fourier = [ [ exp(-im*2*pi*(n-1)*(m-1)/sim_params.L) for n in 1:sim_params.L] for m in 1:sim_params.L] #This is a vector used to fourier transform in one dimension at k = 2pi/L
    new(
      Lattice(sim_params) # Lattice
      ,WormMoveData(sim_params,rng) # Worm Move
      ,sim_params # simulation parameters
      ,MeasurementArray(Any[ObsGreenLoop(sim_params.num_of_measure,sim_params.L)#green function
                            ,ObsZRatio(sim_params.num_of_measure)
                            ,ObsCompressLoop(sim_params.num_of_measure)
                            ,ObsStiffnessLoopAll(sim_params.num_of_measure,sim_params.L)
                            ,ObsEnergy(sim_params.num_of_measure)#energy
                            ,ObsHallConductivity(sim_params.num_of_measure,sim_params.L)#real part of hall conductivity
                            ,ObsCurrentLoop(sim_params.num_of_measure)
                            ,ObsCurrentLoopFT(sim_params.num_of_measure,sim_params.L)
                            # ,ObsStiffnessLoop(sim_params.num_of_measure)
                            # ,ObsStiffnessLoopFT(sim_params.num_of_measure,sim_params.L)
                            ,ObsGreenVillain(sim_params.num_of_measure,sim_params.L)
                            ,ObsCompressVillain(sim_params.num_of_measure)
                            ,ObsCurrentTimeVillain(sim_params.num_of_measure)
                            ,ObsCurrentVillain(sim_params.num_of_measure)
                            ,ObsCurrentVillainFT(sim_params.num_of_measure,sim_params.L)
                            ,ObsStiffnessVillain(sim_params.num_of_measure)
                            ,ObsStiffnessVillainFT(sim_params.num_of_measure,sim_params.L)
                            ,ObsMagnitization(sim_params.num_of_measure)
                            ])
      ,rng
      ,fourier
      ,Disorder(sim_params)
    )
  end
end


function move_J!(sim::Sim)
  closed = (sim.lat.ira==sim.lat.masha)
  if closed
      jump_or_shift=rand(sim.rng)
      if (0.5>jump_or_shift)
        return jump_worm!(sim.worm_data,sim.lat)
      end
  end
  shift_worm!(sim.worm_data,sim.lat,sim.sim_params,sim.disorder)
end



function update_worm_thermal!(sim::Sim)
    for i in 1:(3*sim.sim_params.L^3)
        move_J!(sim)
    end
    return nothing
end

function update_villain!(sim::Sim)
    #we update the villain part by sweeping through all bonds
    for site in eachindex(sim.lat.angle)
        # location = ind2sub(sim.lat.angle,site)
        location = Tuple(CartesianIndices(sim.lat.angle)[site])
        metro_angle!(sim.lat,sim.sim_params,sim.disorder,sim.rng,location)
        metro_p!(sim.lat,sim.sim_params,sim.disorder,sim.rng,location)
    end
    for dir in 1:NDIMS
       # metro_fluctuate!(sim.lat,sim.sim_params,sim.disorder,sim.rng,dir)
    end

  return nothing
end

function update_binding!(sim::Sim)
    #we update the villain part by sweeping through all bonds
    for site in eachindex(sim.lat.angle)
        # location = ind2sub(sim.lat.angle,site)
        location = Tuple(CartesianIndices(sim.lat.angle)[site])
        metro_binding!(sim.lat,sim.sim_params,sim.disorder,sim.rng,location)
    end
  return nothing
end

function check_closed(sim::Sim)
     total_current = 0
     for i in sim.lat.winding
         total_current += abs(i)
     end
    #total_current = abs.(sim.lat.winding[1])+abs.(sim.lat.winding[2])
    return total_current
end

function update_worm!(sim::Sim)
    #we update the villain part by sweeping through all bonds
    while (sim.lat.ira != sim.lat.masha)
    #while (sim.lat.ira != sim.lat.masha) || (check_closed(sim) != 0 )
        for i in 1:(sim.sim_params.num_of_relative_sweeps*sim.sim_params.L^2)
            move_J!(sim)
        end
    end
    return nothing
end

function update_thermal!(sim::Sim)
  #we update the villain part by sweeping through all bonds
  update_villain!(sim)
  update_binding!(sim)
  update_worm_thermal!(sim)
  return nothing
end

function next_move!(sim::Sim)
  update_villain!(sim)
  update_binding!(sim)
  update_worm_thermal!(sim)
  return nothing
end

function thermalize!(sim::Sim)
  for i in 1:sim.sim_params.num_of_thermal
    update_thermal!(sim)
  end
  return nothing
end


function run!(sim::Sim)
    counter = 0
    limit = Int(floor(sim.sim_params.num_of_measure/1))
    # limit = Int(floor(sim.sim_params.num_of_measure))
    while counter < limit
        for c_sweep in 1:sim.sim_params.num_of_sweeps
          next_move!(sim)
        end
        if (sim.lat.ira == sim.lat.masha)
            measure_all!(sim.measurements,sim)
            counter += 1
        elseif (sim.lat.ira != sim.lat.masha)
            measure_correlator!(sim.measurements,sim)
        end
    end
end



end
