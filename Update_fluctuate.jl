module Update_fluctuate

export metro_fluctuate!

using ..SimParameters
using ..Defs
using ..LatticeData
using ..DisorderData

using RandomNumbers

#update the fluctuating angle of Villain model
function metro_fluctuate!(lattice::Lattice,params::SimParams,disorder::Disorder,rng::RandomNumbers.MersenneTwisters.MT19937,dir::Int64)
    angle_change = 2.0*(rand(rng)-0.5)*params.delta_theta
    #There is no binding/ coupling energy related to the update of teh angle variable
    dE = Energy_local_fluctuate(lattice,params,disorder,dir,angle_change)-Energy_local_fluctuate(lattice,params,disorder,dir,0.0)
    if (dE <= 0. || rand(rng) <= exp.(-dE))
        # lattice.angle[location...] = mod1(lattice.angle[location...]+angle_change,2*pi)
        lattice.fluctuate_lattice[dir] = update_angle(lattice.fluctuate_lattice[dir],angle_change)
        lattice.energy += dE
    end
end

function update_angle(angle::Float64,delta::Float64)
    new_angle = mod(angle+delta,2*pi)
    if new_angle > pi
        new_angle = new_angle-2*pi
    end
    return new_angle
end

function Energy_local_fluctuate(lattice::Lattice,params::SimParams,disorder::Disorder,dir::Int64,delta_angle::Float64)
    E_tot=0.0
    new_fluctuate = update_angle(lattice.fluctuate_lattice[dir],delta_angle)/params.L
    for site in eachindex(lattice.angle)
        location = Tuple(CartesianIndices(lattice.angle)[site])
        nn = hop(location,dir,1,size(lattice.angle))
        bond = dir==3 ? params.lambda1 : disorder.bond_disorder_villain[location[1],location[2],dir]
        E = bond*(lattice.angle[nn...]-lattice.angle[location...]-2*pi*lattice.p_lattice[location...,dir]-new_fluctuate)^2. /2.0
        E_tot += E
    end
    # new_fluctuate = update_angle(lattice.fluctuate_lattice[dir],delta_angle)
    # for i in 1:L
    #     for j in 1:L
    #         if dir==1
    #             location=(1,i,j)
    #         elseif dir==2
    #             location=(j,1,i)
    #         elseif dir==3
    #             location=(i,j,1)
    #         end
    #         # new_angle = mod1(lattice.angle[location...]+delta_angle,2*pi)
    #         #by changing an angle we change NDIMS*2 bonds states and energy, so the energy change calculation is looped over them
    #         #for bonds in the XY plane
    #         #disordered bonds are store in Disorder. Due to time time translatio of quenched disorder, we only store bonds of one time slice
    #         nn = hop(location,dir,1,size(lattice.angle))
    #         bond = dir==3 ? params.lambda1 : disorder.bond_disorder_villain[location[1],location[2],dir]
    #         E = bond*(lattice.angle[nn...]-lattice.angle[location...]-2*pi*lattice.p_lattice[location...,dir]-new_fluctuate)^2./2.0
    #         E_tot += E
    #     end
    # end
    return E_tot
end

end
