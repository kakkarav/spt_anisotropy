module Measurements

export Obs,ObsData,MeasurementArray,measure_all!,measure_correlator!,take_measurement!,ObsZRatio,ObsEnergy,ObsHallConductivity,ObsCompressLoop,ObsGreenLoopTime,ObsGreenLoopSpace,ObsStiffnessLoopAll,ObsCurrentLoop,ObsCurrentLoopFT,ObsStiffnessLoop,ObsStiffnessLoopFT,ObsCurrentVillain,ObsCurrentVillainFT,ObsStiffnessVillain,ObsCompressVillain,ObsCurrentTimeVillain,ObsGreenVillainTime,ObsGreenVillainSpace,ObsStiffnessVillainFT,ObsMagnitization


using ..LatticeData
using ..Defs
using LinearAlgebra
using Statistics

# Obsevables

abstract type Obs end

mutable struct ObsData{T}
  name::String
  mean::T
  num_of_measure::Int64
  c_num_of_measure::Int64
end




####################################################################################################################################################################

# ObsDataArray,ObsDataList,ObsDataComplex,ObsDataHall,ObsDataScalar
#data structure

#fixed size array, e.g. correlation function
function ObsDataArray(name::String,num_of_measure::Int64,dims::NTuple{D,Int64}) where {D}
  ObsData{Array{Float64,D}}(name,zeros(dims),0,0)
end

#fixed size array, e.g. correlation function
function ObsDataArrayComplex(name::String,num_of_measure::Int64,dims::NTuple{D,Int64}) where {D}
  ObsData{Array{Complex{Float64},D}}(name,zeros(dims) + im*zeros(dims),0,0)
end

#extensive list, e.g. time series
function ObsDataList(name::String,num_of_measure::Int64)
  ObsData{Array{Float64,1}}(name,zeros(num_of_measure),0,0)
end

#extensive list, e.g. time series
function ObsDataComplex(name::String,num_of_measure::Int64)
  ObsData{Complex{Float64}}(name,0.0+im*0.0,0,0)
end

#extensive list, e.g. time series
function ObsDataHall(name::String,num_of_measure::Int64)
  ObsData{Array{Float64,1}}(name,zeros(num_of_measure),0,0)
end

#scalar
function ObsDataScalar(name::String,num_of_measure::Int64)
  ObsData{Float64}(name,0.0,0,0)
end

####################################################################################################################################################################

# take_measurement!,take_measurement_list!,take_measurement_complex!,take_measurement!
#take measurement

#scalar
function take_measurement!(obs_data::ObsData{T},data::T) where {T}
  obs_data.c_num_of_measure+=1
  obs_data.mean+=(data-obs_data.mean)/float(obs_data.c_num_of_measure)
  return nothing
end

#list of all measurement
function take_measurement_list!(obs_data::ObsData{T},data::Float64) where {T}
  obs_data.c_num_of_measure+=1
  obs_data.mean[obs_data.c_num_of_measure] += data
  return nothing
end

#scalar complex number
function take_measurement_complex!(obs_data::ObsData{T},data::Complex{Float64}) where {T}
  obs_data.c_num_of_measure+=1
  obs_data.mean+=(data-obs_data.mean)/float(obs_data.c_num_of_measure)
  return nothing
end

#data is a list like green function
function take_measurement!(obs_data::ObsData{Array{Float64,D}},data::Array{Float64,D}) where {D}
  obs_data.c_num_of_measure+=1
  for i in eachindex(data)
    obs_data.mean[i]+=(data[i]-obs_data.mean[i])/float(obs_data.c_num_of_measure)
  end
  return nothing
end

function take_measurement!(obs_data::ObsData{Array{Complex{Float64},D}},data::Array{Complex{Float64},D}) where {D}
  obs_data.c_num_of_measure+=1
  for i in eachindex(data)
    obs_data.mean[i]+=(data[i]-obs_data.mean[i])/float(obs_data.c_num_of_measure)
  end
  return nothing
end

#time series: the nth value in the list  is the averge of the measurement from the 1st to the nth measurement.
# function take_measurement_time!{T}(obs_data::ObsData{T},data::Float64)
#   obs_data.c_num_of_measure+=1
#   if obs_data.c_num_of_measure==1
#     obs_data.mean[obs_data.c_num_of_measure] += data
#   else
#      obs_data.mean[obs_data.c_num_of_measure] += (data + (obs_data.c_num_of_measure-1)*obs_data.mean[obs_data.c_num_of_measure-1])/float(obs_data.c_num_of_measure)
#  end
#   return nothing
# end


#####################################################################################################################################################################################################################

# Measurements

mutable struct MeasurementArray
  measurements::Array{Any,1}
  MeasurementArray(measurements::Array{Any,1})=new(measurements)
end

function measure_all!(measurements::MeasurementArray,sim)
  for m in measurements.measurements
    measure_obs!(m,sim)
  end
  return nothing
end

function measure_correlator!(measurements::MeasurementArray,sim)
    for m in 1:2
        measure_obs!(measurements.measurements[m],sim)
    end
  return nothing
end

function get_measurements(m_array::MeasurementArray)
  Dict([(m.obs_data.name,m.obs_data.mean) for m in m_array.measurements])
end

#######################################################################################################################################################################################################################

#Count the number of closed looped# Measure partition function ratio
struct ObsZRatio <:Obs
  obs_data::ObsData{Float64}
  ObsZRatio(num_of_measure)=new(ObsDataScalar("ZRatio",num_of_measure))
end

function measure_obs!(obs_Zratio::ObsZRatio,sim)
  if sim.lat.ira==sim.lat.masha
    closed = 1.0
  else
    closed=0.0
  end
  take_measurement!(obs_Zratio.obs_data,closed)
end


# ObsEnergy,ObsHallConductivity,cal_current_villain_list,cal_angle_villain_list,cal_current_curl_list,cal_current_loop_list,cal_current_disorder_loop_list
# Energy
struct ObsEnergy <:Obs
  obs_data::ObsData{Array{Float64,1}}
  ObsEnergy(num_of_measure)=new(ObsDataList("Energy",num_of_measure))
end

function measure_obs!(obs_E::ObsEnergy,sim)
  E = sim.lat.energy/sim.sim_params.L^(NDIMS-1)/sim.sim_params.Lt
  take_measurement_list!(obs_E.obs_data,E)
  return nothing
end


# Hall conductivity/ Cross specie stiffness
struct ObsHallConductivity <:Obs
  obs_data::ObsData{Complex{Float64}}
  villain_J::Array{Float64,1}
  loop_J::Array{Float64,1}
  coupled_J::Array{Float64,1}
  ObsHallConductivity(num_of_measure,Lt)=new(ObsDataComplex("Complex Hall Conductivity",num_of_measure),zeros(Lt),zeros(Lt),zeros(Lt))
end


function measure_obs!(obs_hall_cond::ObsHallConductivity,sim)
  fill!(obs_hall_cond.villain_J,0.0)
  fill!(obs_hall_cond.loop_J,0.0)
  fill!(obs_hall_cond.coupled_J,0.0)
  for t in 1:sim.sim_params.Lt
      obs_hall_cond.villain_J[t] += cal_current_villain_list(sim,1,t)
      obs_hall_cond.loop_J[t] += cal_current_loop_list(sim,2,t)
      obs_hall_cond.coupled_J[t] += cal_current_disorder_loop_list(sim,2,t)
  end

  current_vill = dot(obs_hall_cond.villain_J,sim.fourier[2])
  current_coupled = dot(obs_hall_cond.coupled_J,sim.fourier[2])*2*im*sin(pi/sim.sim_params.Lt)*exp(-im*pi/sim.sim_params.Lt)/(2*pi)
  # current_coupled = dot(obs_hall_cond.coupled_J,sim.fourier[2])*2*im*sin(pi/sim.sim_params.L)/(2*pi)
  current_loop = conj(dot(obs_hall_cond.loop_J,sim.fourier[2]))
  correlation = (current_vill - current_coupled)*current_loop/sim.sim_params.L^(NDIMS-1)/sim.sim_params.Lt
  correlation = correlation*exp(im*pi/sim.sim_params.Lt)

  take_measurement_complex!(obs_hall_cond.obs_data,correlation)
  return nothing
end


#cal_current_villain_list,cal_angle_villain_list,cal_current_curl_list,cal_current_loop_list,cal_current_disorder_loop_list

#calculate a sum of the current of one time slice (t = time) for Villain model
function cal_current_villain_list(sim, dir::Int64,time::Int64)
    current = 0.0
    for i in 1:size(sim.lat.angle,1)
        for j in 1:size(sim.lat.angle,2)
            location = (i,j,time)
            nn = hop(location,dir,1,size(sim.lat.angle))
            # fluctuate = location[dir]==1 ? sim.lat.fluctuate_lattice[dir] : 0.0
            fluctuate = sim.lat.fluctuate_lattice[dir]/sim.sim_params.Lt
            current += sim.disorder.bond_disorder_villain[i,j,dir]*(sim.lat.angle[nn...]-sim.lat.angle[location...]-2 .*pi*sim.lat.p_lattice[location...,dir]-fluctuate)
        end
    end
    return current
end

function cal_angle_villain_list(sim, dir::Int64,time::Int64)
    angle = 0.0
    for i in 1:size(sim.lat.angle,1)
        for j in 1:size(sim.lat.angle,2)
            location = (i,j,time)
            angle += sim.disorder.bond_disorder_villain[i,j,dir]*sim.lat.angle[location...]
        end
    end
    return angle
end

function cal_current_curl_list(sim, dir::Int64,time::Int64)
    current = 0.0
    for i in 1:size(sim.lat.angle,1)
        for j in 1:size(sim.lat.angle,2)
            current += sim.lat.curl_lattice[i,j,time,dir]
        end
    end
    return current
end

#calculate a sum of the current of one time slice (t = time) for loop model
function cal_current_loop_list(sim, dir::Int64,time::Int64)
    current = 0.0
    for i in 1:size(sim.lat.angle,1)
        for j in 1:size(sim.lat.angle,2)
            current += sim.lat.current[i,j,time,dir]
        end
    end
    return current
end

function cal_current_disorder_loop_list(sim, dir::Int64,time::Int64)
    current = 0.0
    for i in 1:size(sim.lat.angle,1)
        for j in 1:size(sim.lat.angle,2)
            current += (sim.lat.current[i,j,time,dir]-sim.lat.curl_lattice[i,j,time,dir])/sim.disorder.bond_disorder_loop[i,j,dir]
        end
    end
    return current
end

################################################################################################################

#Compressibility: stiffnesss in the imaginary time direction
struct ObsCompressLoop <:Obs
    obs_data::ObsData{Float64}
    ObsCompressLoop(num_of_measure)=new(ObsDataScalar("Compressibility Loop",num_of_measure))
end

# TODO: find the right definition of compressibility in term of beta
function measure_obs!(obs::ObsCompressLoop,sim)
    par_numsq = (sim.lat.winding[end]).^2.0
    compress = par_numsq/sim.sim_params.Lt^NDIMS
    take_measurement!(obs.obs_data,compress)
    return nothing
end

#stiffness loop
struct ObsStiffnessLoopAll <:Obs
     obs_data::ObsData{Array{Float64,1}}
     stiffFT::Array{Float64,1}
     current::Array{Float64,1}
     ObsStiffnessLoopAll(num_of_measure,Lt)=new(ObsDataArray("Stiffness Loop All",num_of_measure,ntuple(x->Lt,1)),zeros(ntuple(x->Lt,1)),zeros(ntuple(x->Lt,1)))
 end

 function measure_obs!(obs::ObsStiffnessLoopAll,sim)
     fill!(obs.stiffFT,0.0)
     for i in 1:sim.sim_params.Lt
         for dir in 1:2
             fill!(obs.current,0.0)
             for t in 1:sim.sim_params.Lt
                 obs.current[t] += cal_current_loop_list(sim,dir,t)
             end
             #println(length(obs.current)==length(sim.fourier[i]))
             obs.stiffFT[i] += abs2.(dot(obs.current,sim.fourier[i])) ./sim.sim_params.L^(NDIMS-1) ./sim.sim_params.Lt ./2
             # if i==1
             #     #divide by 2 for average over x and y
             #     obs.stiffFT[i] += abs2.(dot(obs.current,sim.fourier[i]))./sim.sim_params.L^(NDIMS) ./2
             # else
             #    obs.stiffFT[i] += abs2.(dot(obs.current,sim.fourier[i]))./sim.sim_params.L^(NDIMS-1) ./2. ./pi ./2
             # end
        end
     end
   take_measurement!(obs.obs_data,obs.stiffFT)
   return nothing
 end

#single particle Green's function for loop model
struct ObsGreenLoopTime <:Obs
  obs_data::ObsData{Array{Float64,1}}
  green::Array{Float64,1}
  ObsGreenLoopTime(num_of_measure,Lt)=new(ObsDataArray("Green Loop Time",num_of_measure,ntuple(x->Lt,1)),zeros(ntuple(x->Lt,1)))
end

# TODO: we need to modify so that we can find C(0,t) and C(r,0) separately
function measure_obs!(obs_green::ObsGreenLoopTime,sim)
  fill!(obs_green.green,0.0)
    #we shift the index by 1 because julia start counting from 1 not 0
    delta_worm=mod(sim.lat.ira[3]-sim.lat.masha[3],sim.sim_params.Lt)+1
    if (sim.lat.ira[1]==sim.lat.masha[1]) && (sim.lat.ira[2]==sim.lat.masha[2])
        obs_green.green[delta_worm]+=1.
    end
    # println(obs_green.green)
  take_measurement!(obs_green.obs_data,obs_green.green)
  return nothing
end

struct ObsGreenLoopSpace <:Obs
  obs_data::ObsData{Array{Float64,1}}
  green::Array{Float64,1}
  ObsGreenLoopSpace(num_of_measure,L)=new(ObsDataArray("Green Loop Space",num_of_measure,ntuple(x->L,1)),zeros(ntuple(x->L,1)))
end

# TODO: we need to modify so that we can find C(0,t) and C(r,0) separately
function measure_obs!(obs_green::ObsGreenLoopSpace,sim)
  fill!(obs_green.green,0.0)
    #we shift the index by 1 because julia start counting from 1 not 0
    if (sim.lat.ira[1]==sim.lat.masha[1])
        if (sim.lat.ira[2]==sim.lat.masha[2])
            delta_worm=mod(sim.lat.ira[1]-sim.lat.masha[1],sim.sim_params.Lt)+1
            obs_green.green[delta_worm]+=1.
        elseif (sim.lat.ira[2]==sim.lat.masha[2])
            delta_worm=mod(sim.lat.ira[1]-sim.lat.masha[1],sim.sim_params.Lt)+1
            obs_green.green[delta_worm]+=1.
        end
    end
  take_measurement!(obs_green.obs_data,obs_green.green)
  return nothing
end

#Measure current of loop model
struct ObsCurrentLoop <:Obs
  obs_data::ObsData{Float64}
  ObsCurrentLoop(num_of_measure)=new(ObsDataScalar("Current Loop",num_of_measure))
end

function measure_obs!(obs_current::ObsCurrentLoop,sim)
    current = float(sim.lat.winding[3])/sim.sim_params.L/sim.sim_params.L^(NDIMS/2)
  take_measurement!(obs_current.obs_data,current)
  return nothing
end


#Measure current of loop model
struct ObsCurrentLoopFT <:Obs
    obs_data::ObsData{Complex{Float64}}
    current::Array{Float64,1}
 ObsCurrentLoopFT(num_of_measure,Lt)=new(ObsDataComplex("Current Loop FT",num_of_measure),zeros(Lt))
end

function measure_obs!(obs_current::ObsCurrentLoopFT,sim)
    fill!(obs_current.current,0.0)
    for t in 1:sim.sim_params.Lt
        obs_current.current[t] += cal_current_loop_list(sim,1,t)
    end
    current = dot(obs_current.current,sim.fourier[2])
    current = current/sim.sim_params.L^(NDIMS/2)
  take_measurement_complex!(obs_current.obs_data,current)
  return nothing
end

#Zero-momentum loop stiffness
struct ObsStiffnessLoop <:Obs
    obs_data::ObsData{Float64}
    ObsStiffnessLoop(num_of_measure)=new(ObsDataScalar("Stiffness Loop",num_of_measure))
end

function measure_obs!(obs_stiffness::ObsStiffnessLoop,sim)
    stiffness = 0.
    stiff_dir = 0.
    dir = 1
    for site in eachindex(sim.lat.angle)
      location = Tuple(CartesianIndices(sim.lat.angle)[site])
      stiff_dir += sim.lat.current[location...,dir]
    end
    stiffness += stiff_dir^2/sim.sim_params.L^(NDIMS-1) ./sim.sim_params.Lt
    take_measurement!(obs_stiffness.obs_data,stiffness)
    return nothing
end

#fourier[2] transform version of ObsStiffnessLoop
struct ObsStiffnessLoopFT <:Obs
    obs_data::ObsData{Float64}
    current::Array{Float64,1}
    ObsStiffnessLoopFT(num_of_measure,Lt)=new(ObsDataScalar("Stiffness Loop FT",num_of_measure),zeros(Lt))
end

function measure_obs!(obs_stiff::ObsStiffnessLoopFT,sim)
    fill!(obs_stiff.current,0.0)
    for t in 1:sim.sim_params.Lt
        obs_stiff.current[t] += cal_current_loop_list(sim,1,t)
    end
    stiffness = abs2.(dot(obs_stiff.current,sim.fourier[2]))/sim.sim_params.L^(NDIMS-1) ./sim.sim_params.Lt
    take_measurement!(obs_stiff.obs_data,stiffness)
    return nothing
 end

################################################################################################################


 struct ObsCurrentVillain <:Obs
    obs_data::ObsData{Float64}
   ObsCurrentVillain(num_of_measure)=new(ObsDataScalar("Current Villain",num_of_measure))
 end

 function measure_obs!(obs_stiff::ObsCurrentVillain,sim)
   current = cal_current_villain(sim)
   take_measurement!(obs_stiff.obs_data,current)
   return nothing
 end

 function cal_current_villain(sim)
     current = 0.
     for dir in 1:1
       sum_current = 0.0
       for site in eachindex(sim.lat.angle)
            location = Tuple(CartesianIndices(sim.lat.angle)[site])
            nn = hop(location,dir,1,size(sim.lat.angle))
            bond = sim.disorder.bond_disorder_villain[location[1],location[2],dir]
            sum_current += bond*(sim.lat.angle[nn...]-sim.lat.angle[location...]-2*pi*sim.lat.p_lattice[location...,dir])
       end
       current += sum_current
     end
   return current/sim.sim_params.L^(3 ./2)
 end

#Current of villain_J
struct ObsCurrentVillainFT <:Obs
   obs_data::ObsData{Complex{Float64}}
   current_villain::Array{Float64,1}
   current_loop::Array{Float64,1}
   ObsCurrentVillainFT(num_of_measure,Lt)=new(ObsDataComplex("Current Villain FT",num_of_measure),zeros(Lt),zeros(Lt))
end

function measure_obs!(obs_current::ObsCurrentVillainFT,sim)
    fill!(obs_current.current_villain,0.0)
    fill!(obs_current.current_loop,0.0)
    for t in 1:sim.sim_params.Lt
        obs_current.current_villain[t] += cal_current_villain_list(sim,1,t)
        obs_current.current_loop[t] += cal_current_disorder_loop_list(sim,2,t)
    end
    current_vill = dot(obs_current.current_villain,sim.fourier[2])
    # current_loop = dot(obs_current.current_loop,sim.fourier[2])*2*im*sin(pi/sim.sim_params.L)*exp(-im*pi/sim.sim_params.L)/(2*pi)
    current_loop = dot(obs_current.current_loop,sim.fourier[2])*2*im*sin(pi/sim.sim_params.Lt)
    current = (current_vill-current_loop) ./sim.sim_params.L ./sim.sim_params.Lt^(1. ./2.)

  take_measurement_complex!(obs_current.obs_data,current)
  return nothing
end

struct ObsStiffnessVillain <:Obs
   obs_data::ObsData{Float64}
  ObsStiffnessVillain(num_of_measure)=new(ObsDataScalar("Stiffness Villain",num_of_measure))
end

function measure_obs!(obs_stiff::ObsStiffnessVillain,sim)
  stiffness = cal_stiffness_villain(sim)
  take_measurement!(obs_stiff.obs_data,stiffness)
  return nothing
end

function cal_stiffness_villain(sim)
    stiffness = 0.0
    sum_current = 0.0
    dir = 1
    for site in eachindex(sim.lat.angle)
        location = Tuple(CartesianIndices(sim.lat.angle)[site])
        nn = hop(location,dir,1,size(sim.lat.angle))
        bond = sim.disorder.bond_disorder_villain[location[1],location[2],dir]
        # fluctuate = location[dir]==1 ? sim.lat.fluctuate_lattice[dir] : 0.0
        fluctuate = sim.lat.fluctuate_lattice[dir]/sim.sim_params.L
        sum_current += bond*(sim.lat.angle[nn...]-sim.lat.angle[location...]-2*pi*sim.lat.p_lattice[location...,dir]-fluctuate)
    end
    stiffness += (sim.disorder.sum_villain_bond[1]*sim.sim_params.L-sum_current^2)/sim.sim_params.L^(NDIMS-1) ./sim.sim_params.Lt
    return stiffness
end

struct ObsCompressVillain <:Obs #compressibility
   obs_data::ObsData{Float64}
  ObsCompressVillain(num_of_measure)=new(ObsDataScalar("Compressibility Villain",num_of_measure))
end

function measure_obs!(obs_stiff::ObsCompressVillain,sim)
  compressibility = cal_compressibility_villain(sim)
  take_measurement!(obs_stiff.obs_data,compressibility)
  return nothing
end

function cal_compressibility_villain(sim)
    stiffness = 0.0
    sum_current = 0.0
    dir = 3
    for site in eachindex(sim.lat.angle)
        location = Tuple(CartesianIndices(sim.lat.angle)[site])
        # nn = hop(location,dir,1,size(sim.lat.angle))
        bond = sim.sim_params.lambda1
        # fluctuate = location[dir]==1 ? sim.lat.fluctuate_lattice[dir] : 0.0
        fluctuate = sim.lat.fluctuate_lattice[dir]/sim.sim_params.L
        # sum_current += bond*(sim.lat.angle[nn...]-sim.lat.angle[location...]-2*pi*sim.lat.p_lattice[location...,dir]-fluctuate)
        sum_current += bond*(-2*pi*sim.lat.p_lattice[location...,dir]-fluctuate)
    end
    stiffness += (sim.sim_params.lambda1*(sim.sim_params.L)^3-sum_current^2)/sim.sim_params.L^(NDIMS-1) ./sim.sim_params.Lt
    return stiffness
end

#calculate the current in the time direction
struct ObsCurrentTimeVillain <:Obs
   obs_data::ObsData{Float64}
  ObsCurrentTimeVillain(num_of_measure)=new(ObsDataScalar("Current Time Villain",num_of_measure))
end

function measure_obs!(obs_stiff::ObsCurrentTimeVillain,sim)
  current = cal_current_time_villain(sim)
  take_measurement!(obs_stiff.obs_data,current)
  return nothing
end

function cal_current_time_villain(sim)
    current = 0.
    sum_current = 0.0
    dir = 3
    for site in eachindex(sim.lat.angle)
       location = Tuple(CartesianIndices(sim.lat.angle)[site])
       nn = hop(location,dir,1,size(sim.lat.angle))
       bond = sim.sim_params.lambda1
       sum_current += bond*(sim.lat.angle[nn...]-sim.lat.angle[location...]-2*pi*sim.lat.p_lattice[location...,dir])
    end
    current += sum_current
  return current ./sim.sim_params.L ./sim.sim_params.Lt^(1/2)
end



# TODO: find the separate green function for C(r,0) and C(r,t)
#single particle Green's function for villain part

struct ObsGreenVillainTime <:Obs
  obs_data::ObsData{Array{Complex{Float64},1}}
  green::Array{Complex{Float64},1}
  ObsGreenVillainTime(num_of_measure,Lt)=new(ObsDataArrayComplex("Green Villain Time",num_of_measure,ntuple(x->Lt,1)),zeros(ntuple(x->Lt,1)))
end

function measure_obs!(obs_green::ObsGreenVillainTime,sim)
  fill!(obs_green.green,0.0 + im*0.0)
  for t in 1:sim.sim_params.Lt
      for j in eachindex(sim.lat.angle)
        j_site = Tuple(CartesianIndices(sim.lat.angle)[j])
        new_time = mod1(j_site[3]+t-1,sim.sim_params.Lt)
        obs_green.green[t]+=exp.(im*(sim.lat.angle[j_site[1],j_site[2],new_time]-sim.lat.angle[j_site...]))
      end
  end
  take_measurement!(obs_green.obs_data,obs_green.green)
  return nothing
end

#single particle Green's function for villain part
# struct ObsGreenVillainTime <:Obs
#   obs_data::ObsData{Array{Complex{Float64},1}}
#   green::Array{Complex{Float64},1}
#   ObsGreenVillainTime(num_of_measure,Lt)=new(ObsDataArrayComplex("Green Villain Time",num_of_measure,ntuple(x->Lt,1)),zeros(ntuple(x->Lt,1)))
# end

# function measure_obs!(obs_green::ObsGreenVillainTime,sim)
#   fill!(obs_green.green,0.0 + im*0.0)
#   center_mag = cal_mag_villain_list(sim,1)
#   for t in 1:sim.sim_params.Lt
#       obs_green.green[t] += (conj(cal_mag_villain_list(sim,t)) .*center_mag)
#       # obs_green.green[t] += (conj(cal_mag_villain_list(sim,t)) .*center_mag)/(sim.sim_params.L)^3.0
#       # obs_green.green[t] += 0.0
#   end
#   take_measurement!(obs_green.obs_data,obs_green.green)
#   return nothing
# end

struct ObsGreenVillainSpace <:Obs
  obs_data::ObsData{Array{Complex{Float64},1}}
  green::Array{Complex{Float64},1}
  ObsGreenVillainSpace(num_of_measure,L)=new(ObsDataArrayComplex("Green Villain Space",num_of_measure,ntuple(x->L,1)),zeros(ntuple(x->L,1)))
end

function measure_obs!(obs_green::ObsGreenVillainSpace,sim)
  fill!(obs_green.green,0.0 + im*0.0)
  for r in 1:sim.sim_params.L
      for j in eachindex(sim.lat.angle)
        j_site = Tuple(CartesianIndices(sim.lat.angle)[j])
        new_x = mod1(j_site[1]+r,sim.sim_params.L)
        obs_green.green[r]+=exp.(im*(sim.lat.angle[new_x,j_site[2],j_site[3]]-sim.lat.angle[j_site...]))/2
        new_y = mod1(j_site[2]+r,sim.sim_params.L)
        obs_green.green[r]+=exp.(im*(sim.lat.angle[j_site[1],new_y,j_site[3]]-sim.lat.angle[j_site...]))/2
      end
  end
  take_measurement!(obs_green.obs_data,obs_green.green)
  return nothing
end


function cal_mag_villain_list(sim,time::Int64)
    total_mag = 0.0 + im*0.0
    for i in 1:size(sim.lat.angle,1)
        for j in 1:size(sim.lat.angle,2)
            total_mag += exp.(im*sim.lat.angle[i,j,time])
        end
    end
    return total_mag
end



#Fourier transform version of ObsStiffnessVillain
struct ObsStiffnessVillainFT <:Obs
    obs_data::ObsData{Float64}
    current_villain::Array{Float64,1}
    current_loop::Array{Float64,1}
    ObsStiffnessVillainFT(num_of_measure,Lt)=new(ObsDataScalar("Stiffness Villain FT",num_of_measure),zeros(Lt),zeros(Lt))
end

function measure_obs!(obs_stiff::ObsStiffnessVillainFT,sim)
    fill!(obs_stiff.current_villain,0.0)
    fill!(obs_stiff.current_loop,0.0)
    for t in 1:sim.sim_params.Lt
        obs_stiff.current_villain[t] += cal_current_villain_list(sim,1,t)
        obs_stiff.current_loop[t] += cal_current_disorder_loop_list(sim,2,t)
    end
    current_vill = dot(obs_stiff.current_villain,sim.fourier[2])
    # current_loop = dot(obs_stiff.current_loop,sim.fourier)*2*im*sin(pi/sim.sim_params.L)*exp(-im*pi/sim.sim_params.L)/(2*pi)
    current_loop = dot(obs_stiff.current_loop,sim.fourier[2])*2*im*sin(pi/sim.sim_params.Lt)/(2*pi)

    correlation = abs2.(current_vill-current_loop)

    stiffness = ((sim.disorder.sum_villain_bond[1]*sim.sim_params.L+4*sin(pi/sim.sim_params.Lt)^2.0*sim.disorder.sum_loop_bond_inverse[2]*sim.sim_params.Lt/(2*pi)^2)-correlation) ./sim.sim_params.L^(NDIMS-1) ./sim.sim_params.Lt
    take_measurement!(obs_stiff.obs_data,stiffness)
    return nothing
 end

struct ObsMagnitization <:Obs
  obs_data::ObsData{Float64}
  ObsMagnitization(num_of_measure)=new(ObsDataScalar("Magnitization",num_of_measure))
end

function measure_obs!(obs_mag::ObsMagnitization,sim)
    tot_mag = 0.0+ im*0.0
    for site in eachindex(sim.lat.angle)
         location = Tuple(CartesianIndices(sim.lat.angle)[site])
         tot_mag += exp.(im*sim.lat.angle[location...])
    end
  magnitization = abs.(tot_mag) ./sim.sim_params.L^(NDIMS-1) ./sim.sim_params.Lt
  take_measurement!(obs_mag.obs_data,magnitization)
  return nothing
end


end
