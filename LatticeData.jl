module LatticeData

export Lattice,hop,test_configuration,curl,test_energy,find_energy,energy_error
using ..Defs
using ..SimParameters
using ..DisorderData

using Base.Cartesian

mutable struct Lattice
#angle and P variable of Villain model
  angle::Array{Float64,NDIMS}
  p_lattice::Array{Int64,NDIMS+1}
  #current variable of the loop model
  current::Array{Int64,NDIMS+1}
  #curl lattice is the lattice of the curl of the p variable
  curl_lattice::Array{Int64,NDIMS+1}
  #fluctuating boundary
  fluctuate_lattice::Array{Float64,1}
  # Ira and Masha
  ira::NTuple{NDIMS,Int64}
  masha::NTuple{NDIMS,Int64}
  # tot winding
  winding::Array{Int64,1}
  energy::Float64
  function Lattice(sim_params::SimParams)
    angle_lattice = zeros(Float64,sim_params.L*ones(Int64,NDIMS-1)...,sim_params.Lt)
    fluctuate_lattice = zeros(Float64,NDIMS)
    p_lattice = zeros(Int64,sim_params.L*ones(Int64,NDIMS-1)...,sim_params.Lt,NDIMS)
    current_lattice = zeros(Int64,sim_params.L*ones(Int64,NDIMS-1)...,sim_params.Lt,NDIMS)
    curl_lattice = zeros(Int64,sim_params.L*ones(Int64,NDIMS-1)...,sim_params.Lt,NDIMS)
    new(angle_lattice#angle lattice
    ,p_lattice#p_lattice
    ,current_lattice#current
    ,curl_lattice#curl_lattice
    ,fluctuate_lattice#fluctuating boundary lattice
    ,tuple(ones(Int64,NDIMS)...) # Ira
    ,tuple(ones(Int64,NDIMS)...) # Masha
    ,zeros(Int64,NDIMS) # tot winding
    ,0.#energy
    )
  end
end

@generated function hop(index::NTuple{NDIMS,Int64},dir::Int64,lr::Int64,dims::NTuple{NDIMS,Int64})
    quote
        @nif 2 lr_d->(lr==lr_d) lr_d->
        begin
            @nif $NDIMS d->(d==dir) d->
            begin
                @ntuple $NDIMS d_t->
                begin
                    if d_t==d
                      if (lr_d==1)
                        (index[d_t]==dims[d_t] ? 1 : index[d_t]+1)
                      elseif (lr_d==2)
                        (index[d_t]==1 ? dims[d_t] : index[d_t]-1)
                      else
                        print(" lr is out of bound")
                      end
                    else
                      index[d_t]
                    end
                end
            end
        end
    end
end


#curl funtion for 3D, dir is the component of our interest
function curl(lattice::Lattice,location::NTuple{NDIMS,Int64},dir::Int64)
    #this function returns curl at location of the loop lattice
    #cyclic is a cyclic list of numbers 123
    villain_site = hop(location, dir, 1, size(lattice.angle))
    n = cyclic[dir+1]
    nn = cyclic[dir+2]
    return derivative(lattice,villain_site,n,nn)-derivative(lattice,villain_site,nn,n)
end


function derivative(lattice::Lattice,location::NTuple{NDIMS,Int64},dir::Int64,value::Int64)
    # we use forward definition (hopping in the 1 direction) for derivative to make our variable convention consistent
  nn = hop(location,dir,1,size(lattice.angle))
  return lattice.p_lattice[nn...,value]-lattice.p_lattice[location...,value]
end


#test if the curl lattice only have closed loops/ is divergenceless
function test_divergence_curl_lattice(lat::Lattice,sim_params::SimParams)
  for i in eachindex(lat.angle)
    tot_current_local=0
    # c_site=ind2sub(size(lat.angle),i)
    c_site = Tuple(CartesianIndices(lat.angle)[i])
    for d in 1:NDIMS
      for lr in 1:2
        site = lr ==1 ? c_site : hop(c_site,d,2,size(lat.angle))
        charge = ( lr==1 ? 1 : -1)
        tot_current_local += charge*lat.curl_lattice[site...,d]
      end
    end
    if tot_current_local!= 0
      println("Falied at ",c_site," non-zero flux")
      # c_site=ind2sub(size(lat.angle),i)
      c_site = Tuple(CartesianIndices(lat.angle)[i])
      @show c_site
      @show tot_current_local
      return false
    end
  end
end

#test if the current lattice only have closed loops/ is divergenceless
function test_divergence_current(lat::Lattice,sim_params::SimParams)
  for i in eachindex(lat.angle)
    tot_current_local=0
    c_site = Tuple(CartesianIndices(lat.angle)[i])
    for d in 1:NDIMS
      for lr in 1:2
        site = lr ==1 ? c_site : hop(c_site,d,2,size(lat.angle))
        charge = ( lr==1 ? 1 : -1)
        tot_current_local += charge*lat.current[site...,d]
      end
    end

    if tot_current_local!= 0
      println("Falied at ",c_site," non-zero flux")
      c_site = Tuple(CartesianIndices(lat.angle)[i])
      @show c_site
      @show lat.ira
      @show lat.masha
      @show tot_current_local
      return false
    end
  end
  return true
end

#test if the update is consistent with the forward curl defnition
function test_curl(lat::Lattice,sim_params::SimParams)
  for i in eachindex(lat.angle)
     #loop coordinate
    c_site = Tuple(CartesianIndices(lat.angle)[i])
    for dir in 1:NDIMS
        c = curl(lat,c_site,dir)
      if c != lat.curl_lattice[c_site...,dir]
        println("Curl falied at ",c_site)
        println(c," ",lat.curl_lattice[c_site...,dir], " ",dir)
        # @show dir
        # @show c
        # @show lat.curl_lattice[c_site...,dir]
        return false
      end
    end
  end
end

#check if Stoke theorem is obeyed, i.e. curl_P = intergral of loop in P variable around curl_P vector
function test_stoke(lat::Lattice,sim_params::SimParams)
  for i in eachindex(lat.angle)
    c_site = Tuple(CartesianIndices(lat.angle)[i])
    for dir1 in 1:NDIMS
        #now integrate/sum along the loops
        temp = 0
        dir2 = cyclic[dir1+1]
        dir3 = cyclic[dir1+2]

        loop_site = hop(c_site, dir1, 1, size(lat.angle))

        temp  += lat.p_lattice[loop_site...,dir2]
        temp  -= lat.p_lattice[loop_site...,dir3]

        jump1 = hop(loop_site, dir2, 1, size(lat.angle))
        temp  += lat.p_lattice[jump1...,dir3]

        jump2 = hop(loop_site, dir3, 1, size(lat.angle))
        temp  -= lat.p_lattice[jump2...,dir2]


        # for dir in 1:NDIMS
        #   if dir != dir1
        #     for off in 1:2
        #         #too obscure
        #     site = off==1 ? c1 : hop(c1, odd[dir,dir1], 1, size(lat.angle))
        #     charge = mod1(dir-dir1,NDIMS) + off
        #     temp += lat.p_lattice[site...,dir] * (-1)^charge
        #     end
        #   end
        # end
        if temp != lat.curl_lattice[c_site...,dir1]
        println("Stoke falied at ",c_site," ",temp, " ",lat.curl_lattice[c_site...,dir1], " ",dir1)

        return false
        end
    end
  end
end

function test_angle(lat::Lattice,sim_params::SimParams)
  for dir in 1:NDIMS
      if abs.(lat.fluctuate_lattice[dir]) > (pi+0.0000001)
        println("Angle out of bound")
      end
  end

  for i in eachindex(lat.angle)
    site = Tuple(CartesianIndices(lat.angle)[i])
    if abs.(lat.angle[site...]) > (pi+0.0000001)
        println("Angle out of bound")
    end
  end
end


#calculate energy for a particular bond. I label the bond by location and direction
function energy(lat::Lattice,params::SimParams,disorder::Disorder,location::NTuple{NDIMS,Int64},dir::Int64)
  nn = hop(location,dir,1,size(lat.angle))
  angle = lat.angle
  fluctuate = location[dir]==3 ? lat.fluctuate_lattice[dir]/params.Lt : lat.fluctuate_lattice[dir]/params.L
  # fluctuate = lat.fluctuate_lattice[dir]/params.L
  p_lattice = lat.p_lattice
  lambda1 = dir==NDIMS ? params.lambda1 : disorder.bond_disorder_villain[location[1],location[2],dir]
  lambda2 = dir==NDIMS ? params.lambda2 : disorder.bond_disorder_loop[location[1],location[2],dir]
  E_villain = (lambda1/2.0)*(angle[nn...] .-angle[location...] .-2*pi*(p_lattice[location...,dir]) .-fluctuate)^2
  E_loop = 1/(2.0*lambda2)*(lat.current[location...,dir]-params.inter*curl(lat,location,dir))^2
  #E_loop = 1/(2.0*lambda2)*(lat.current[location...,dir]-params.inter*lat.curl_lattice[location...,dir])^2
  E_loop_self = 1/(2.0*params.lambda3)*(lat.current[location...,dir])^2
  E_disorder = dir==NDIMS ? disorder.chemical_potential[location[1],location[2]]*lat.current[location...,dir] : 0.0
  E =  E_villain+E_loop+E_loop_self+E_disorder
  return E
end


function find_energy(lat::Lattice,sim_params::SimParams,disorder::Disorder)
  #check the energy)
    E = 0.
  for j in eachindex(lat.angle)
    j_site = Tuple(CartesianIndices(lat.angle)[j])
    for dir in 1:NDIMS
      E += energy(lat,sim_params,disorder,j_site,dir)
    end
  end
  return E
end

#test if energy is correct. This funcion calculate the energy directly from the lattice
function test_energy(lat::Lattice,sim_params::SimParams,disorder::Disorder)
  #check the energy
  E = 0.
  for j in eachindex(lat.angle)
    j_site = Tuple(CartesianIndices(lat.angle)[j])
    for dir in 1:NDIMS
      E += energy(lat,sim_params,disorder,j_site,dir)
    end
  end
  #our toloerance for the error is 10.0^(-11)
  if abs(E - lat.energy) > 10.0^(-11)
    println("Real energy: ", E," Lattice energy: ", lat.energy)
    println("energy diff = ", E - lat.energy)
  end
  return nothing
end

function energy_error(sim)
    #check the energy
    E = 0.
    lat = sim.lat
    sim_params = sim.sim_params
    disorder = sim.disorder
    for j in eachindex(lat.angle)
        j_site = Tuple(CartesianIndices(lat.angle)[j])
        for dir in 1:NDIMS
            E += energy(lat,sim_params,disorder,j_site,dir)
        end
    end
    return abs(E - lat.energy)/lat.energy
end

#below are two test wrapper used to test the configuration
function test_configuration(Sim)
  lat = Sim.lat
  sim_params = Sim.sim_params
  disorder = Sim.disorder
  if (sim_params.inter==1)
    test_divergence_curl_lattice(lat,sim_params)
    test_curl(lat,sim_params)
    test_stoke(lat,sim_params)
  end
  test_divergence_current(lat,sim_params)
  test_energy(lat,sim_params,disorder)
  test_angle(lat,sim_params)
end


end
