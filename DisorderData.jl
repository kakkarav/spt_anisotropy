module DisorderData

export Disorder

using ..Defs
using ..SimParameters
using RandomNumbers

struct Disorder#chemical potential lattices

    chemical_potential::Array{Float64,NDIMS-1}
    chemical_weight::Array{Float64,NDIMS-1}
    bond_disorder_villain::Array{Float64,NDIMS}
    bond_disorder_loop::Array{Float64,NDIMS}
    weights_inc::Array{Array{Float64,1},NDIMS}
    weights_dec::Array{Array{Float64,1},NDIMS}
    sum_villain_bond::Array{Float64,1}
    sum_loop_bond::Array{Float64,1}
    sum_loop_bond_inverse::Array{Float64,1}

    function Disorder(sim_params::SimParams)
        rng=RandomNumbers.MersenneTwisters.MT19937(sim_params.seed)
        #chemical_potential = rand(rng,sim_params.L*ones(Int64,NDIMS-1)...)
        chemical_potential = rand(sim_params.L*ones(Int64,NDIMS-1)...)
        chemical_potential = (chemical_potential*2.0 .- 1)*sim_params.chem_disorder .+ sim_params.chem_potential
        prob = sim_params.prob
        #chemical_potential = map(x -> (x < prob) ? sim_params.chem_disorder*rand([-1,1]) : 0 , rand(sim_params.L*ones(Int64,NDIMS-1)...))

        bond_struct = sim_params.bond_type

        #prob = sim_params.prob
        #prob = 0.0

        if (bond_struct == 1)
            #bond_disorder_villain = 2*rand(rng,sim_params.L*ones(Int64,NDIMS-1)...,2) .-1
            bond_disorder_loop = 2*rand(rng,sim_params.L*ones(Int64,NDIMS-1)...,2) .-1
            #bond_disorder_villain = (1.0 .+bond_disorder_villain*sim_params.bond_disorder_villain).*sim_params.lambda1
            bond_disorder_villain = (1.0 .+bond_disorder_loop*sim_params.bond_disorder_loop).*sim_params.lambda1
            bond_disorder_loop = (1.0 .+bond_disorder_loop*sim_params.bond_disorder_loop).*sim_params.lambda2
        elseif (bond_struct == 2)
            bond_disorder_villain = map(x -> (x < prob) ? sim_params.lambda1 ./3. : sim_params.lambda1 , rand(rng,sim_params.L*ones(Int64,NDIMS-1)...,2))
            bond_disorder_loop = map(x -> (x < prob) ? sim_params.lambda2 ./3. : sim_params.lambda2 , rand(rng,sim_params.L*ones(Int64,NDIMS-1)...,2))
        end

        chemical_weight= exp.(-chemical_potential./sim_params.lambda2)
        weights_inc,weights_dec=calc_ratios(sim_params,bond_disorder_loop)
        sum_villain_bond = [sum(bond_disorder_villain[:,:,1]),sum(bond_disorder_villain[:,:,2])]
        sum_loop_bond = [sum(1 ./bond_disorder_loop[:,:,1]),sum(1 ./bond_disorder_loop[:,:,2])]
        sum_loop_bond_inverse = [sum(1.0 ./bond_disorder_loop[:,:,1]),sum(1.0 ./bond_disorder_loop[:,:,2])]

        new(chemical_potential
        ,chemical_weight
        ,bond_disorder_villain
        ,bond_disorder_loop
        ,weights_inc
        ,weights_dec
        ,sum_villain_bond
        ,sum_loop_bond
        ,sum_loop_bond_inverse
        )
    end
end

function calc_ratios(sim_params::SimParams,bond_loop::Array{Float64,NDIMS})
    w_inc = Array{Array{Float64,1}}(undef,sim_params.L,sim_params.L,NDIMS-1)
    w_dec = Array{Array{Float64,1}}(undef,sim_params.L,sim_params.L,NDIMS-1)

    for site in eachindex(bond_loop)
        #this might be wrong because we add another dimension that is not space in bond_loop
        # location = ind2sub(bond_loop,site)
        location = Tuple(CartesianIndices(bond_loop)[site])

        coupling = 1/(2. *bond_loop[location...])

        energy_inc = coupling*[ ((n+1)^2. -n^2.) for n in -MAX_BOND_J:MAX_BOND_J ]
        weights_inc= exp.(-energy_inc)

        energy_dec = coupling*[ ((n-1)^2. -n^2.) for n in -MAX_BOND_J:MAX_BOND_J ]
        weights_dec= exp.(-energy_dec)

        w_inc[location...] = weights_inc
        w_dec[location...] = weights_dec
    end

  return w_inc,w_dec
end




end #module
