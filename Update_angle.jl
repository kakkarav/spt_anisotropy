module Update_angle

export metro_angle!

using ..SimParameters
using ..Defs
using ..LatticeData
using ..DisorderData

using RandomNumbers

#update the angle of Villain model
function metro_angle!(lattice::Lattice,params::SimParams,disorder::Disorder,rng::RandomNumbers.MersenneTwisters.MT19937,location::NTuple{NDIMS,Int64})
  angle_change = 2.0*(rand(rng)-0.5)*params.delta_theta
  #There is no binding/ coupling energy related to the update of teh angle variable
  dE = Energy_local_angle(lattice,params,disorder,location,angle_change)-Energy_local_angle(lattice,params,disorder,location,0.0)
  if (dE <= 0. || rand(rng) <= exp.(-dE))
    # E_before = lattice.energy
    # lattice.angle[location...] = mod1(lattice.angle[location...]+angle_change,2*pi)
    lattice.angle[location...] = update_angle(lattice.angle[location...],angle_change)
    lattice.energy += dE

    # E_after = find_energy(lattice,params,disorder)
    # if (abs.((E_after-E_before)- dE) > 10.^(-10.))
    #   println("wrong angle update")
    # end
  end
  return nothing
end

function update_angle(angle::Float64,delta::Float64)
    new_angle = mod(angle+delta,2*pi)
    if new_angle > pi
        new_angle = new_angle-2*pi
    end
    return new_angle
end

function Energy_local_angle(lattice::Lattice,params::SimParams,disorder::Disorder,location::NTuple{NDIMS,Int64},delta_angle::Float64)
    # new_angle = mod1(lattice.angle[location...]+delta_angle,2*pi)
    new_angle = update_angle(lattice.angle[location...],delta_angle)
    #by changing an angle we change NDIMS*2 bonds states and energy, so the energy change calculation is looped over them
    E = 0.
    fluataute = 0.0
    #for bonds in the XY plane
    #disordered bonds are store in Disorder. Due to time time translatio of quenched disorder, we only store bonds of one time slice
    for dir in 1:2
      for lc in 1:2
          #we hop here in order to calculate \omega_{\mu} = phi(r+\mu) - phi(r)
        nn = hop(location,dir,lc,size(lattice.angle))
        if lc ==1
          # fluctuate = location[dir]==1 ? lattice.fluctuate_lattice[dir] : 0.0
          fluctuate = lattice.fluctuate_lattice[dir]/params.L
          E += disorder.bond_disorder_villain[location[1],location[2],dir]*(lattice.angle[nn...]-new_angle-2*pi*lattice.p_lattice[location...,dir]-fluctuate)^2.
        elseif lc ==2
          # fluctuate = nn[dir]==1 ? lattice.fluctuate_lattice[dir] : 0.0
          fluctuate = lattice.fluctuate_lattice[dir]/params.L
          E += disorder.bond_disorder_villain[nn[1],nn[2],dir]*(new_angle-lattice.angle[nn...]-2*pi*lattice.p_lattice[nn...,dir]-fluctuate)^2.
        end
      end
    end

    #for bond in the time direction (dir==3)
    for lc in 1:2
        #we hop here in order to calculaet \omega_{\mu} = phi(r+\mu) - phi(r)
      nn = hop(location,NDIMS,lc,size(lattice.angle))
      if lc ==1
        # fluctuate = location[3]==1 ? lattice.fluctuate_lattice[3] : 0.0
        fluctuate = lattice.fluctuate_lattice[3]/params.Lt
        E += params.lambda1*(lattice.angle[nn...]-new_angle-2*pi*lattice.p_lattice[location...,NDIMS]-fluctuate)^2.
      elseif lc ==2
        # fluctuate = nn[3]==1 ? lattice.fluctuate_lattice[3] : 0.0
        fluctuate = lattice.fluctuate_lattice[3]/params.Lt
        E += params.lambda1*(new_angle-lattice.angle[nn...]-2*pi*lattice.p_lattice[nn...,NDIMS]-fluctuate)^2.
      end
    end

    return E/2.0
end

end
