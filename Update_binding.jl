module Update_binding

export metro_binding!

using ..SimParameters
using ..Defs
using ..LatticeData
using ..Coupling
using ..Update_p
using ..DisorderData
using RandomNumbers
using Base.Cartesian



#this is a special update that decide to update P and current variable at the same time.
#by changing P, we create a loop in curl variable and a loop in current variable in the same/opposite direction
function metro_binding!(lat::Lattice,params::SimParams,disorder::Disorder,rng::RandomNumbers.MersenneTwisters.MT19937,location::NTuple{NDIMS,Int64})
  for dir in 1:NDIMS
      random_move = rand(rng,binding_range)
      # p_change
      p_change = mod1(random_move,3)-2
      # loop direction (positive loop_change is creating a looping in the same direction as positive p_change,
      # i.e. if both loop_chaange and p_change have the same sign, there is no energy change)
      # TODO:all the all 6 possiblity to create all the loops
      loop_change = fld1(random_move,3)-2
      if (p_change != 0 || loop_change != 0)
          dE_1 = energy_local_villain(lat,params,disorder,location,p_change,dir)
          dE_2 = energy_coupling_old(lat,params,disorder,location,p_change,loop_change,dir)
          # dE_3 = energy_coupling_self(lat,params,disorder,location,loop_change,dir)+energy_loop_disorder(lat,params,disorder,location,loop_change,dir)
          dE_3 = energy_loop_disorder(lat,params,disorder,location,loop_change,dir)
          dE = dE_1+dE_2+dE_3
          if (dE <= 0. || rand(rng) <= exp.(-dE))
            # print((p_change,loop_change))
            # lat.energy += dE
            lat.energy += dE
            lat.p_lattice[location...,dir] += p_change
            update_c!(lat,location,p_change,dir) #update curl variable
            update_binding!(lat,location,loop_change,dir) #update current loop
          end
      end
  end
end


end
