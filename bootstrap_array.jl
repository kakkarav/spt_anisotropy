include("Defs.jl")
include("SimParameters.jl")
include("DisorderData.jl")
include("LatticeData.jl")
include("Update_angle.jl")
include("Update_fluctuate.jl")
include("Update_J.jl")
include("Coupling.jl")
include("Update_p.jl")
include("Update_binding.jl")
include("Measurements.jl")
include("Simulation_array.jl")
using .SimParameters
using .Simulation_array
using .Measurements
using .LatticeData
using Statistics


function PrintArray(array)
    word = string(array[1])
    for i in 2:length(array)
        word = string(word," ",real(array[i]))
    end
    return word
end

function print_output(meas::Dict{String,Any},step::Int64,c_num::Int64)
    Energylist = meas["Energy"][1:c_num]
    E = (mean(Energylist))
    HeatCap = (std(Energylist))^2.
    RealCond = (real(meas["Complex Hall Conductivity"]))
    ImagCond = (imag(meas["Complex Hall Conductivity"]))

    StiffL = (meas["Stiffness Loop All"][1])
    StiffLFT = (meas["Stiffness Loop All"][2:end])
    CurrL = (meas["Current Loop"])
    CurrLFT_re = (real(meas["Current Loop FT"]))
    CurrLFT_im = (imag(meas["Current Loop FT"]))
    CompressL = (meas["Compressibility Loop"])
    GreenLT = (meas["Green Loop Time"])
    GreenLS = (meas["Green Loop Space"])
    ZRatio = meas["ZRatio"]
    #Green = (meas["Stiffness Loop All"][2:end])

    GreenVT = (real(meas["Green Villain Time"]))
    GreenVTErr = (imag(meas["Green Villain Time"]))
    GreenVS = (real(meas["Green Villain Space"]))
    GreenVSErr = (imag(meas["Green Villain Space"]))
    CompressV = (meas["Compressibility Villain"])
    CurrTV = (meas["Current Time Villain"])
    CurrV = (meas["Current Villain"])
    StiffV = (meas["Stiffness Villain"])
    StiffVFT = (meas["Stiffness Villain FT"])
    #CurrV = (meas["Current Villain"])
    CurrVFT_re = (real(meas["Current Villain FT"]))
    CurrVFT_im = (imag(meas["Current Villain FT"]))
    mag = (meas["Magnitization"])


    return println(string(step), ",",string(E), ",",string(HeatCap), ",",string(RealCond), ",",string(ImagCond), ",", string(StiffL), ",",
    PrintArray(StiffLFT), ",", string(CurrL), ",", string(CurrLFT_re), ",", string(CurrLFT_im), ",", string(StiffV), ",",
    string(StiffVFT), ",", string(CurrTV), ",", string(CurrVFT_re), ",", string(CurrVFT_im), ",", string(mag), ",", string(CompressL), ",",
    PrintArray(GreenLT), ",", PrintArray(GreenVT), ",", PrintArray(GreenVTErr), ",", string(CompressV), ",", string(ZRatio), ",", string(CurrV), ",",
    PrintArray(GreenLS), ",", PrintArray(GreenVS), ",", PrintArray(GreenVSErr))

end


function monte_carlo(sim_dict::Dict{String,Real})
    sim_params=SimParameters.SimParams_new(sim_dict)
    sim=Simulation_array.Sim(sim_params)
    Simulation_array.thermalize!(sim)
    for step in 1:100
        Simulation_array.run!(sim)
        meas=Measurements.get_measurements(sim.measurements)
        c_num = sim.measurements.measurements[3].obs_data.c_num_of_measure
        print_output(meas,step,c_num)
        #println("c_num = ",sim.measurements.measurements[1].obs_data.c_num_of_measure)
    end
    return nothing
end


opt_file = open(ARGS[1], "r")
file_data = readlines( opt_file)

dual_lambda = 0
dual_L = 0
input_params = Dict{String,Real}()
for i=1:size(file_data)[1]
  #println(file_data[i],)
  params = split(file_data[i], ",")
  #println(params)
  if params[1] == "lambda1"||params[1] == "lambda2"||params[1] == "lambda3"||params[1] == "delta_theta"||params[1] == "chem_potential"||params[1] == "chem_disorder"||params[1] == "bond_disorder_villain"||params[1] == "bond_disorder_loop"||params[1] == "prob"
    input_params[params[1]] = parse(Float64, params[2])
elseif params[1] == "dual_lambda"
     global dual_lambda = parse(Int64, params[2])
 elseif params[1] == "dual_L"
    global dual_L = parse(Int64, params[2])
  else
    input_params[params[1]] = parse(Int64, params[2])
  end
end

if (dual_lambda==1)
    input_params["lambda2"] = input_params["lambda1"]
end

if (dual_L==1)
    input_params["Lt"] = input_params["L"]
end

println(string("----BEGIN PARAMS----"))
println("lambda1,", input_params["lambda1"])
println("lambda2,", input_params["lambda2"])
println("lambda3,", input_params["lambda3"])
println("inter,", input_params["inter"])
println("delta_theta,", input_params["delta_theta"])
println("chemical_potential,", input_params["chem_potential"])
println("bond_type,", input_params["bond_type"])
println("prob,", input_params["prob"])
println("chemical_disorder,", input_params["chem_disorder"])
println("bond_disorder_villain,", input_params["bond_disorder_villain"])
println("bond_disorder_loop,", input_params["bond_disorder_loop"])


println("seed,", input_params["seed"])
println("L,", input_params["L"])
println("Lt,", input_params["Lt"])

println("num_of_thermal,", input_params["num_of_thermal"])
println("num_of_measure,", input_params["num_of_measure"])
println("num_of_sweeps,", input_params["num_of_sweeps"])
println("num_of_relative_sweeps,", input_params["num_of_relative_sweeps"])

println("----BEGIN DATA-----")
println("Step,Energy,HeatCapacity,RealConductivity,ImagConductivity,StiffLoop,StiffLoopFT,CurrentLoop,CurrentLoopFTRe,CurrentLoopFTIm,StiffVillain,StiffVillainFT,CurrentVillain,CurrentVillainFTRe,CurrentVillainFTIm,Magnitization,CompressibilityL,Green,CurrentVillain")

monte_carlo(input_params)
